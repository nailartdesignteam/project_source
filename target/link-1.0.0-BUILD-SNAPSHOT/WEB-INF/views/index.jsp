<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="css/main/style.css" type="text/css" />
<link rel="stylesheet" href="css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="css/user.css" type="text/css" />
<link rel="stylesheet" href="js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="js/prismjs/prism.css"  data-noprefix />

</head>  
<body id="body">	
	
	<nav class="nav--light nav--gray nav--absolute">
		<div class="container">
			<div class="row row--nav row--nav--baseline">
				<div class="nav-mod logo left">
					<a href="index.html"><img alt="" src="images/logo--white.png" height="22"></a>
				</div>
				<div class="nav-mod nav-toggle right visible-xs visible-sm">
					<i class="material-icons pointer">&#xE5D2;</i>
				</div>
				<div class="nav-mod nav hidden-xs hidden-sm left">
					<ul class="menu text--uppercase">
						
						<li class="hr visible-xs visible-sm">
							<form><input type="search" placeholder="Type and hit enter to search"></form>
						</li>
						<li>
							<a href="rush-documentation.html">Elements</a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="elements-accordion.html">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
								<li>
							    	<a href="elements-backgrounds.html">Backgrounds</a>
								</li>
								<li>
							    	<a href="elements-baners.html">Baners</a>
								</li>
								<li>
							    	<a href="elements-boxes.html">Boxes</a>
								</li>
								<li>
							    	<a href="elements-breadcrumbs.html">Breadcrumbs & pagination</a>
								</li>
								<li>
									<a href="elements-buttons.html">Buttons</a>
								</li>
								<li>
							    	<a href="elements-cards.html">Cards</a>
								</li>		
							    <li>
							    	<a href="elements-colors.html">Colors</a>
								</li>
								<li>
							    	<a href="elements-сomments.html">Comments</a>
								</li>
								<li>
							    	<a href="elements-countdown.html">Countdown</a>
								</li>
								<li>
							    	<a href="elements-dropdowns.html">Dropdowns</a>
								</li>
								<li>
									<a href="elements-forms.html">Forms</a>
								</li>	
								<li>
							    	<a href="elements-grid.html">Grid</a>
								</li>
								<li>
							    	<a href="elements-history.html">History</a>
								</li>
								<li>
							    	<a href="elements-hovers.html">Hovers</a>
								</li>
								<li>
							    	<a href="elements-icons.html">Icons</a>
								</li>	
								<li>
							    	<a href="elements-image-modal.html">Image modal</a>
								</li>
								<li>
							    	<a href="elements-social.html">Instagram feeds</a>
								</li>
								<li>
							    	<a href="elements-lists.html">Lists</a>
								</li>
								<li>
							    	<a href="elements-mailchimp.html">MailChimp</a>
								</li>
								<li>
							    	<a href="elements-maps.html">Maps</a>
								</li>
								<li>
							    	<a href="elements-masonry.html">Masonry</a>
								</li>
								<li>
							    	<a href="elements-mixitup.html">Mix It</a>
								</li>
								<li>
							    	<a href="elements-modals.html">Modals</a>
								</li>
								<li>
							    	<a href="elements-navigations.html">Navigations</a>
								</li>
								<li>
							    	<a href="elements-page-navigation.html">Page navigation</a>
								</li>
								<li>
							    	<a href="elements-parallax.html">Parallax</a>
								</li>
								<li>
							    	<a href="elements-progress.html">Progress</a>
								</li>
								<li>
							    	<a href="elements-slider.html">Slider</a>
								</li>
								<li>
							    	<a href="elements-swipe.html">Swipe content</a>
								</li>
								<li>
							    	<a href="elements-tables.html">Tables</a>
								</li>
								<li>
							    	<a href="elements-tabs.html">Tabs</a>
								</li>
								<li>
							    	<a href="elements-sectiontoggle.html">Toggle Section</a>
								</li>
								<li>
							    	<a href="elements-typed.html">Typed text</a>
								</li>
								<li>
									<a href="elements-typography.html">Typography</a>
								</li>
								<li>
							    	<a href="elements-video.html">Video</a>
								</li>
								<li>
							    	<a href="elements-video-bg.html">Video background</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Pages</a>
							<div class="container menu__dropdown menu__dropdown--hero boxed-lg">
									<div class="row">
										<div class="col-md-8 col-sm-12 col-xs-12">
											<div class="row marg-b-30">
												<div class="col-md-6 col-sm-6 col-xs-12">
													<ul class="ul--split">
														<li class="menu__dropdown__title">
															<b>Home</b>
														</li>
														<li>
															<a href="home-architecture.html">Architecture</a>
														</li>
														<li>
															<a href="home-brand.html">Brand</a>
														</li>
														<li>
															<a href="home-dubai-rtl.html">RTL demo</a>
														</li>
														<li>
															<a href="home-video-dark.html">Video</a>
														</li>
														<li>
															<a href="home-portfolio-dark.html">Portfolio</a>
														</li>
														<li>
															<a href="home-portfolio-white.html">Portfolio #2</a>
														</li>
														<li>
															<a href="home-portfolio-fullscreen.html">Portfolio fullscreen</a>
														</li>
														<li>
															<a href="home-resume-dark.html">Resume</a>
														</li>
														<li>
															<a href="home-photographer-dark.html">Photographer</a>
														</li>
														<li>
															<a href="home-photographer-white.html">Photographer #2</a>
														</li>
														<li>
															<a href="home-company.html">Company</a>
														</li>
														<li>
															<a href="home-company-2.html">Company #2</a>
														</li>
														<li>
															<a href="home-company-3.html">Company #3</a>
														</li>
														<li>
															<a href="home-company-4.html">Company #4</a>
														</li>
														<li>
															<a href="home-company-5.html">Company #5</a>
														</li>
														<li>
															<a href="home-company-6.html">Company #6</a>
														</li>
														<li>
															<a href="home-travel.html">Travel</a>
														</li>
														<li>
															<a href="home-other.html">Other</a>
														</li>
														<li>
															<a href="home-sport.html">Sport</a>
														</li>
														<li>
															<a href="home-studio.html">Studio</a>
														</li>
														<li>
															<a href="home-studio-2.html">Studio #2</a>
														</li>
														<li>
															<a href="home-restaurant.html">Restaurant</a>
														</li>
													</ul>
													<ul class="ul--split">
														<li class="menu__dropdown__title">
															<b>Specialty Pages</b>
														</li>
														<li>
															<a href="specialty-account.html">Account</a>
														</li>
														<li>
															<a href="specialty-login.html">Login</a>
														</li>
														<li>
															<a href="specialty-create.html">Create</a>
														</li>
														<li>
															<a href="specialty-recover.html">Recover</a>
														</li>
														
														
														<li>
															<a href="specialty-comingsoon.html">Coming Soon</a>
														</li>
														<li>
															<a href="specialty-comingsoon-background.html">Coming Soon #2</a>
														</li>
														<li>
															<a href="specialty-search-results.html">Search results</a>
														</li>
														<li>
															<a href="specialty-service.html">Service mode</a>
														</li>
														<li>
															<a href="specialty-404.html">404</a>
														</li>
													</ul>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-12">
													<ul>
														<li class="menu__dropdown__title">
															<b>Landings</b>
														</li>
														<li>
															<a href="landing-app.html">App</a>
														</li>
														<li>
															<a href="landing-app-2.html">App #2</a>
														</li>
														<li>
															<a href="landing-app-3.html">App #3</a>
														</li>
														<li>
															<a href="landing-app-4.html">App #4</a>
														</li>
														<li>
															<a href="landing-company.html">Company</a>
														</li>
														<li>
															<a href="landing-company-2.html">Company #2</a>
														</li>
														<li>
															<a href="landing-company-3.html">Company #3</a>
														</li>
														<li>
															<a href="landing-studio.html">Studio</a>
														</li>
														<li>
															<a href="landing-sport.html">Sport</a>
														</li>
														<li>
															<a href="landing-service.html">Service</a>
														</li>
														<li>
															<a href="landing-service-2.html">Service</a>
														</li>
														<li>
															<a href="landing-travel.html">Travel</a>
														</li>
														<li>
															<a href="landing-travel-2.html">Travel 2</a>
														</li>
													</ul>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-12">
													<ul>
														<li class="menu__dropdown__title">
															<b>Second Pages</b>
														</li>
														<li>
															<a href="page-our-company.html">Our company</a>
														</li>
														<li>
															<a href="page-history.html">History</a>
														</li>
														<li>
															<a href="page-support.html">Support</a>
														</li>
														<li>
															<a href="page-faq.html">FAQ</a>
														</li>
														<li>
															<a href="page-price.html">Price</a>
														</li>
														<li>
															<a href="page-price-2.html">Price #2</a>
														</li>
														<li>
															<a href="page-article.html">Article</a>
														</li>
														<li>
															<a href="page-article-2.html">Article #2</a>
														</li>
														<li>
															<a href="page-article-3.html">Article #3</a>
														</li>
														<li>
															<a href="page-article-4.html">Article #4</a>
														</li>
														<li>
															<a href="page-contact-us.html">Contact us</a>
														</li>
														<li>
															<a href="page-contact-us-2.html">Contact us #2</a>
														</li>
														<li>
															<a href="page-contact-us-3.html">Contact us #3</a>
														</li>
														<li>
															<a href="page-contact-us-4.html">Contact us #4</a>
														</li>
													</ul>
												</div>
											</div>
											<div class="row hidden-xs">
												<div class="col-md-11 col-sm-12 col-xs-12">
													<form><input type="search" placeholder="Type and hit enter to search"></form>
												</div>
											</div>
										</div>
									</div>
									<div class="bg pos-absolute pos-right pos-top col-md-4 hidden-xs hidden-sm" style="background: url('images/hi/kari-shea-226157.jpg');">
										<div class="boxed-lg pos-absolute pos-top">
											
										</div>
										<div class="boxed-lg pos-absolute pos-bottom">
											
										</div>
									</div>
							</div>
						</li>
						<li>
							<a href="#">Blog</a>
							<ul class="menu__dropdown boxed">
								<li>
									<a href="#">Home <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="blog-home-masonry.html">Masonry</a>
										</li>
										<li>
											<a href="blog-home-boxed.html">Boxed sidebar</a>
										</li>
										<li>
											<a href="blog-home-boxed-fluid.html">Boxed fluid</a>
										</li>
										<li>
											<a href="blog-home-default.html">Default</a>
										</li>
										<li>
											<a href="blog-home-column.html">Column</a>
										</li>
										<li>
											<a href="blog-home-column-2.html">Column #2</a>
										</li>
										<li>
											<a href="blog-home-fluid.html">Fluid</a>
										</li>
									</ul>
								</li>	
								<li>
									<a href="#">Post <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="blog-post-1.html">Post #1</a>
										</li>
										<li>
											<a href="blog-post-2.html">Post #2</a>
										</li>
										<li>
											<a href="blog-post-3.html">Post #3</a>
										</li>
										<li>
											<a href="blog-post-4.html">Post #4</a>
										</li>
										<li>
											<a href="blog-post-5.html">Post #5</a>
										</li>
										<li>
											<a href="blog-post-6.html">Post #6</a>
										</li>
										<li>
											<a href="blog-post-7.html">Post #7</a>
										</li>
									</ul>
								</li>								
							</ul>
						</li>
						<li>
							<a href="#">Shop</a>
							<ul class="menu__dropdown boxed">
								<li>
								<a href="#">Main <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="shop-main.html">Main</a>
										</li>
										<li>
											<a href="shop-main-2.html">Main #2</a>
										</li>
										<li>
											<a href="shop-main-3.html">Main #3</a>
										</li>
									</ul>
								</li>
								<li>
								<a href="#">Catalog boxed <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="shop-catalog-boxed-4.html">4 Columns</a>
										</li>
										<li>
											<a href="shop-catalog-boxed-3.html">3 Columns</a>
										</li>
										<li>
											<a href="shop-catalog-boxed-3-sidebar.html">3 Columns + Sidebar</a>
										</li>
										<li>
											<a href="shop-catalog-boxed-2-sidebar.html">2 Columns + Sidebar</a>
										</li>
										<li>
											<a href="shop-catalog-boxed-2-minimal.html">2 Columns minimal</a>
										</li>
									</ul>
								</li>
								<li>
								<a href="#">Catalog default <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="shop-catalog-default-4.html">4 Columns</a>
										</li>
										<li>
											<a href="shop-catalog-default-3.html">3 Columns</a>
										</li>
										<li>
											<a href="shop-catalog-default-3-sidebar.html">3 Columns + Sidebar</a>
										</li>
										<li>
											<a href="shop-catalog-default-2-sidebar.html">2 Columns + Sidebar</a>
										</li>
										<li>
											<a href="shop-catalog-default-2-minimal.html">2 Columns minimal</a>
										</li>
									</ul>
								</li>
								<li class="hr">
								<a href="#">Product page <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="shop-card-default.html">Default</a>
										</li>
										<li>
											<a href="shop-card-detail.html">Detail</a>
										</li>
									</ul>
								</li>
								
								<li> 
									<a href="shop-cart.html">Cart</a>
								</li>
								<li> 
									<a href="shop-cart-shipping.html">Shipping</a>
								</li>
								<li> 
									<a href="shop-cart-payment.html">Payment</a>
								</li>
								<li> 
									<a href="shop-cart-success.html">Checkout success</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">Portfolio</a>
							<ul class="menu__dropdown boxed">
								<li class="menu__dropdown__title">
									<b>Cards</b>
								</li>									
								<li>
									<a href="#">Boxed <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="portfolio-boxed-3.html">3 Columns</a>
										</li>
										<li>
											<a href="portfolio-boxed-2.html">2 Columns</a>
										</li>
										<li>
											<a href="portfolio-boxed-1.html">Big Cards</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="#">Hover <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="portfolio-hover-3.html">3 Columns</a>
										</li>
										<li>
											<a href="portfolio-hover-2.html">2 Columns</a>
										</li>
										<li>
											<a href="portfolio-hover-1.html">Big Cards</a>
										</li>
									</ul>
								</li>
								<li class="hr">
									<a href="#">Default <i class="material-icons hidden-xs hidden-sm right"></i> <i class="material-icons  hidden-md  hidden-lg right"></i></a>
									
									<ul class="menu__dropdown boxed">									
										<li>
											<a href="portfolio-default-3.html">3 Columns</a>
										</li>
										<li>
											<a href="portfolio-default-2.html">2 Columns</a>
										</li>
										<li>
											<a href="portfolio-default-1.html">Big Cards</a>
										</li>
									</ul>
								</li>
								<li class="menu__dropdown__title">
									<b>Fluid</b>
								</li>
								<li>
									<a href="portfolio-fluid.html">Default</a>
								</li>
								<li>
									<a href="portfolio-fluid-big.html">Big</a>
								</li>
								<li>
									<a href="portfolio-fluid-grid-with-sidebar.html">Big with sidebar</a>
								</li>
								<li class="hr">
									<a href="portfolio-fluid-grid.html">Grid</a>
								</li>
								<li class="menu__dropdown__title">
									<b>Project pages</b>
								</li>
								<li>
									<a href="portfolio-project-boxed.html">Boxed</a>
								</li>
								<li>
									<a href="portfolio-project-sidebar.html">Sidebar</a>
								</li>
								<li>
									<a href="portfolio-project-sidebar-2.html">Sidebar #2</a>
								</li>
								<li>
									<a href="portfolio-project-default.html">Default</a>
								</li>
								
							</ul>
						</li>
					</ul>
				</div>
				<div class="nav-mod-group right left-xs left-sm hidden-xs">
					<div class="nav-mod">
						<a href="https://themeforest.net/item/rush-multipurpose-html-with-page-builder/19905370" class="btn btn-sm btn--default">Buy now</a>
					</div>
					<div class="nav-mod">
						<a href="builder.html" target="_blank" class="btn btn-sm btn--line">Try Builder</a>
					</div>
				</div>
			</div>
		</div>
	</nav>
	
	<section class="bg-color--main">
		<div class="container">
			<div class="row marg-t-120 marg-b-120 text-center">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h1 class="h3 marg-b-30">Build your pages with Rush template. <br>It’s <span class="typed color--main" data-speed="300" data-repeat="true">easy!, fast!</span></h1>
					<p class="h6">100+ pages</p>
					<a href="rush-documentation.html" class="btn btn-sm btn--line">Documentation</a>
				</div>		
			</div>
		</div>
	</section>
	
	<section class="bg-color--gray tabs">
		<div class="container">
			<div class="row" data-translate="0, -107px">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<ul class="tabs__links hr">
						<li data-tab-link="1" class="active"><b>Home<small> – 22</small></b></li>
						<li data-tab-link="2"><b>Landings<small> – 13</small></b></li>
						<li data-tab-link="3"><b>Pages<small> – 14</small></b></li>
						<li data-tab-link="4"><b>Blog<small> – 14</small></b></li>
						<li data-tab-link="5"><b>Shop<small> – 19</small></b></li>
						<li data-tab-link="6"><b>Portfolio<small> – 17</small></b></li>
						<li data-tab-link="7"><b>Specialty<small> – 9</small></b></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container tabs__content active" data-tab-content="1"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-architecture.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-architecture.png" alt=""><p class="marg-b-30">Home Page<br><b>Architecture</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company-6.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-company-6.png" alt=""><p class="marg-b-30">Home Page<br><b>Company #6</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-dubai-rtl.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-dubai-rtl.png" alt=""><p class="marg-b-30">Home Page<br><b>RTL Demo</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-sport.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-sport.png" alt=""><p class="marg-b-30">Home Page<br><b>Sport</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-studio-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-studio-2.png" alt=""><p class="marg-b-30">Home Page<br><b>Studio #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="home-brand.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/home-brand.png" alt=""><p class="marg-b-30">Home Page<br><b>Brand</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="home-video-dark.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/video-dark.png" alt=""><p class="marg-b-30">Home Page<br><b>Video</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-portfolio-dark.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-dark.png" alt=""><p class="marg-b-30">Home Page<br><b>Portfolio Dark</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-portfolio-white.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-white.png" alt=""><p class="marg-b-30">Home Page<br><b>Portfolio White</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-portfolio-fullscreen.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-fullscreen.png" alt=""><p class="marg-b-30">Home Page<br><b>Portfolio Fullscreen</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-resume-dark.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/resume-dark.png" alt=""><p class="marg-b-30">Home Page<br><b>Resume Dark</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-photographer-dark.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/photographer-dark.png" alt=""><p class="marg-b-30">Home Page<br><b>Photographer Dark</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-photographer-white.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/photographer-white.png" alt=""><p class="marg-b-30">Home Page<br><b>Photographer White</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/company.png" alt=""><p class="marg-b-30">Home Page<br><b>Company</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/company-2.png" alt=""><p class="marg-b-30">Home Page<br><b>Company 2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/company-3.png" alt=""><p class="marg-b-30">Home Page<br><b>Company 3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/company-4.png" alt=""><p class="marg-b-30">Home Page<br><b>Company 4</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-company-5.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/company-5.png" alt=""><p class="marg-b-30">Home Page<br><b>Company 5</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-travel.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/travel.png" alt=""><p class="marg-b-30">Home Page<br><b>Travel</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-other.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/other.png" alt=""><p class="marg-b-30">Home Page<br><b>Other</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-studio.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/studio.png" alt=""><p class="marg-b-30">Home Page<br><b>Studio</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="home-restaurant.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/restaurant.png" alt=""><p class="marg-b-30">Home Page<br><b>Restaurants</b></p></a>
				</div>
			</div>
		</div>

		<div class="container tabs__content" data-tab-content="2"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-service-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-service-2.png" alt=""><p class="marg-b-30">Home Page<br><b>Service #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="landing-travel-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-travel-2.png" alt=""><p class="marg-b-30">Landing<br><b>Travel #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="landing-app-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-app-4.png" alt=""><p class="marg-b-30">Landing<br><b>App #4</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="landing-app.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-app.png" alt=""><p class="marg-b-30">Landing<br><b>App</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-app-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-app-2.png" alt=""><p class="marg-b-30">Landing<br><b>App #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-app-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-app-3.png" alt=""><p class="marg-b-30">Landing<br><b>App #3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-company.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-company.png" alt=""><p class="marg-b-30">Landing<br><b>Company</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-sport.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-sport.png" alt=""><p class="marg-b-30">Landing<br><b>Sport</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-company-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-company-2.png" alt=""><p class="marg-b-30">Landing<br><b>Company #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-company-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-company-3.png" alt=""><p class="marg-b-30">Landing<br><b>Company #3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-studio.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-studio.png" alt=""><p class="marg-b-30">Landing<br><b>Studio</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-service.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-service.png" alt=""><p class="marg-b-30">Landing<br><b>Service</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="landing-travel.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/landing-travel.png" alt=""><p class="marg-b-30">Landing<br><b>Travel</b></p></a>
				</div>
			</div>
		</div>
		
		<div class="container tabs__content" data-tab-content="3"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-article-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-article-4.png" alt=""><p class="marg-b-30">Page<br><b>Article #4</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-our-company.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-our-company.png" alt=""><p class="marg-b-30">Page<br><b>Our company</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-history.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-history.png" alt=""><p class="marg-b-30">Page<br><b>History</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-support.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-support.png" alt=""><p class="marg-b-30">Page<br><b>Support</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-faq.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-faq.png" alt=""><p class="marg-b-30">Page<br><b>FAQ</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-price.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-price.png" alt=""><p class="marg-b-30">Page<br><b>Price</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-price-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-price-2.png" alt=""><p class="marg-b-30">Page<br><b>Price #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-article.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-article.png" alt=""><p class="marg-b-30">Page<br><b>Article</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-article-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-article-2.png" alt=""><p class="marg-b-30">Page<br><b>Article #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-article-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-article-3.png" alt=""><p class="marg-b-30">Page<br><b>Article #3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-contact-us.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-contact-us.png" alt=""><p class="marg-b-30">Page<br><b>Contact us</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-contact-us-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-contact-us-2.png" alt=""><p class="marg-b-30">Page<br><b>Contact us #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-contact-us-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-contact-us-3.png" alt=""><p class="marg-b-30">Page<br><b>Contact us #3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="page-contact-us-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/page-contact-us-4.png" alt=""><p class="marg-b-30">Page<br><b>Contact us #4</b></p></a>
				</div>
			</div>
		</div>
	
		<div class="container tabs__content" data-tab-content="6"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-fluid-grid-with-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-fluid-grid-with-sidebar.png" alt=""><p class="marg-b-30">Portfolio<br><b>Fluid big with sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="portfolio-boxed-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-boxed-3.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Boxed 3/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-boxed-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-boxed-2.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Boxed 2/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-boxed-1.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-boxed-1.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Boxed Big col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="portfolio-hover-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-hover-3.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Hover 3/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-hover-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-hover-2.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Hover 2/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-hover-1.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-hover-1.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards Hover Big col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="portfolio-default-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-default-3.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards default 3/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-default-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-default-2.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards default 2/Col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-default-1.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-default-1.png" alt=""><p class="marg-b-30">Portfolio<br><b>Cards default Big col</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-fluid.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-fluid.png" alt=""><p class="marg-b-30">Portfolio<br><b>Fluid default</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-fluid-big.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-fluid-big.png" alt=""><p class="marg-b-30">Portfolio<br><b>Fluid big</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-fluid-grid.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-fluid-grid.png" alt=""><p class="marg-b-30">Portfolio<br><b>Fluid grid</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-project-boxed.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-project-boxed.png" alt=""><p class="marg-b-30">Portfolio project<br><b>Boxed</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-project-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-project-sidebar.png" alt=""><p class="marg-b-30">Portfolio project<br><b>Sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-project-sidebar-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-project-sidebar-2.png" alt=""><p class="marg-b-30">Portfolio project<br><b>Sidebar #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="portfolio-project-default.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/portfolio-project-default.png" alt=""><p class="marg-b-30">Portfolio project<br><b>Default</b></p></a>
				</div>
				
			</div>
		</div>
		
		<div class="container tabs__content" data-tab-content="4"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="blog-home-column-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-column-2.png" alt=""><p class="marg-b-30">Blog home<br><b>One column with sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="blog-post-6.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-6.png" alt=""><p class="marg-b-30">Blog post<br><b>One column with sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="blog-home-boxed-fluid.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-boxed-fluid.png" alt=""><p class="marg-b-30">Blog home<br><b>Boxed fluid</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="blog-post-7.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-7.png" alt=""><p class="marg-b-30">Blog post<br><b>#7</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-home-masonry.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-masonry.png" alt=""><p class="marg-b-30">Blog home<br><b>Masonry</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-home-boxed.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-boxed.png" alt=""><p class="marg-b-30">Blog home<br><b>Boxed sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-home-default.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-default.png" alt=""><p class="marg-b-30">Blog home<br><b>Default</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-home-column.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-column.png" alt=""><p class="marg-b-30">Blog home<br><b>One column</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-home-fluid.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-home-fluid.png" alt=""><p class="marg-b-30">Blog home<br><b>Fluid</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-post-1.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-1.png" alt=""><p class="marg-b-30">Blog post<br><b>#1</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-post-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-2.png" alt=""><p class="marg-b-30">Blog post<br><b>#2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-post-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-3.png" alt=""><p class="marg-b-30">Blog post<br><b>#3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-post-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-4.png" alt=""><p class="marg-b-30">Blog post<br><b>#4</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="blog-post-5.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/blog-post-5.png" alt=""><p class="marg-b-30">Blog post<br><b>#5</b></p></a>
				</div>
				
			</div>
		</div>
		
		<div class="container tabs__content" data-tab-content="5"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="shop-main-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-main-3.png" alt=""><p class="marg-b-30">Shop<br><b>Main #3</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-main.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-main.png" alt=""><p class="marg-b-30">Shop<br><b>Main</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-main-2.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-main-2.png" alt=""><p class="marg-b-30">Shop<br><b>Main #2</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-boxed-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-boxed-4.png" alt=""><p class="marg-b-30">Shop catalog<br><b>4 Columns</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-boxed-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-boxed-3.png" alt=""><p class="marg-b-30">Shop catalog<br><b>3 Columns</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-boxed-3-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-boxed-3-sidebar.png" alt=""><p class="marg-b-30">Shop catalog<br><b>3 Columns Sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-boxed-2-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-boxed-2-sidebar.png" alt=""><p class="marg-b-30">Shop catalog<br><b>2 Columns Sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-boxed-2-minimal.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-boxed-2-minimal.png" alt=""><p class="marg-b-30">Shop catalog<br><b>2 Columns ninimal</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-default-4.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-default-4.png" alt=""><p class="marg-b-30">Shop catalog<br><b>4 Columns</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-default-3.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-default-3.png" alt=""><p class="marg-b-30">Shop catalog<br><b>3 Columns</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-default-3-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-default-3-sidebar.png" alt=""><p class="marg-b-30">Shop catalog<br><b>3 Columns Sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-default-2-sidebar.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-default-2-sidebar.png" alt=""><p class="marg-b-30">Shop catalog<br><b>2 Columns Sidebar</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-catalog-default-2-minimal.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-catalog-default-2-minimal.png" alt=""><p class="marg-b-30">Shop catalog<br><b>2 Columns ninimal</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-card-default.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-card-default.png" alt=""><p class="marg-b-30">Shop card<br><b>Default</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-card-detail.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-card-detail.png" alt=""><p class="marg-b-30">Shop card<br><b>Detail</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-cart.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-cart.png" alt=""><p class="marg-b-30">Shop card</p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-cart-shipping.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-cart-shipping.png" alt=""><p class="marg-b-30">Shop card<br><b>Shipping</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-cart-payment.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-cart-payment.png" alt=""><p class="marg-b-30">Shop card<br><b>Payment</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="shop-cart-success.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/shop-cart-success.png" alt=""><p class="marg-b-30">Shop card<br><b>Success</b></p></a>
				</div>
			</div>
		</div>
		
		<div class="container tabs__content" data-tab-content="7"> <!-- container--swipe  container--swipe--animate container--swipe--visiblearea -->
			<div class="row grid">
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover text-center">
					<a href="specialty-comingsoon-background.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-comingsoon-background.png" alt=""><p class="marg-b-30">Specialty<br><b>Comingsoon #2</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-account.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-account.png" alt=""><p class="marg-b-30">Specialty<br><b>Account</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-login.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-login.png" alt=""><p class="marg-b-30">Specialty<br><b>Login</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-create.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-create.png" alt=""><p class="marg-b-30">Specialty<br><b>Create</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-recover.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-recover.png" alt=""><p class="marg-b-30">Specialty<br><b>Recover</b></p></a>
				</div>
				
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-comingsoon.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-comingsoon.png" alt=""><p class="marg-b-30">Specialty<br><b>Comingsoon</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-search-results.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-search-results.png" alt=""><p class="marg-b-30">Specialty<br><b>Search results</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-service.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-service.png" alt=""><p class="marg-b-30">Specialty<br><b>Service</b></p></a>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 grid-item hover grid-sizer text-center">
					<a href="specialty-404.html"><img class="boxed--radius boxed--shadow hover__animate--up" src="images/works/specialty-404.png" alt=""><p class="marg-b-30">Specialty<br><b>404</b></p></a>
				</div>
			</div>
		</div>
		
	</section>
	
	<footer class="bg-color--gray">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 text-center">
					<a href="index.html"><img alt="" src="images/logo--white.png" height="22"></a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12 text-center">
					<p class="p-xs text--opacity-5">Copyright © 2016 Rush All rights reserved.</p>
				</div>
			</div>
		</div>
	</footer>
	<section class="pos-fixed pos-left pos-right pos-bottom overflow-visible width--full padding-null">
		<div class="container-fluid">
			<div class="row offset-b-15">
				<div class="col-sm-2 col-xs-12 right marg-null">
					<span class="dropdown dropdown--right dropdown--top dropdown--toggle-on-click right">
						<b class="dropdown__toggle"><i class="material-icons ico--radial ico--radial--fill-light boxed--shadow-xs">mail_outline</i></b>
						<div class="dropdown__content dropdown__content--2 bg-color--white boxed--shadow-lg">
							<form>
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 offset-b-15">
										<h5 class="text-left">Rush Feedback <i class="dropdown__content__close material-icons right pointer ico-xs">clear</i></h5>
									</div>
									<div class="col-md-12 col-sm-12 col-xs-12">
										<input type="email" placeholder="Email *" name="email" class="valid" data-alert="Please write your e-mail">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<textarea placeholder="Comment" name="comment"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 padding-null">
										<input type="submit" class="btn btn--default btn--stretched" data-mailto="info@aristarx.com" value="Send">
									</div>
								</div>
							</form>
						</div>
					</span>
				</div>		
			</div>
		</div>
	</section>



<script type="text/javascript" src="js/jquery-git.min.js"></script>
<script type="text/javascript" src="js/instafeed.js"></script>
<script type="text/javascript" src="js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/picker/picker.js"></script>
<script type="text/javascript" src="js/picker/picker.date.js"></script>
<script type="text/javascript" src="js/prismjs/prism.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/animate.js"></script>
<script type="text/javascript" src="js/waypoints.js"></script>
<script type="text/javascript" src="js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/typed.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript" src="js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="js/jquery.mobile-git.min.js"></script>

</body>

</html>

