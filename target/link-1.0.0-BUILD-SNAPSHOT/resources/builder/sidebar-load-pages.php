<?php
include 'common.php';	
?>

<ul class="ul--list tabs__content active tabs__content--user-pages" data-tab-content="1">
<?php

$directory = $_POST['folder']; // .'/';
function listPages($path, $name)
{
    global $directory;
    if ($name == 'builder-blank-page' || $name == '404' || $name == 'index' || $name == 'builder' || $name == 'wave' || $name == 'login' || $name == 'builder-documentation' || $name == 'rush-documentation' || $name == 'documentation' || $name == 'main' || mb_substr($name, 0, 9) == 'elements-' || mb_substr($name, 0, 5) == 'home-' || mb_substr($name, 0, 5) == 'blog-' || mb_substr($name, 0, 5) == 'shop-' || mb_substr($name, 0, 5) == 'page-' || mb_substr($name, 0, 10) == 'portfolio-' || mb_substr($name, 0, 10) == 'specialty-' || mb_substr($name, 0, 8) == 'landing-' || mb_substr($name, 0, 9) == 'sections-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listElements($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 9) != 'elements-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listHome($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 5) != 'home-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listBlog($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 5) != 'blog-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listShop($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 5) != 'shop-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listPage($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 5) != 'page-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listPortfolio($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 10) != 'portfolio-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listSpecialty($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 10) != 'specialty-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

function listLanding($path, $name)
{
	
    global $directory;
    if (mb_substr($name, 0, 8) != 'landing-')
        return;

    echo '
	<li class="app__sidebar-pages__page" data-name="'.$name.'" data-directory="'.$directory.'">
		<div>
			<span class="pages__page-select">'.$name.'</span>
			<span class="right">
				<a href="'.$name.'.html" target="_blank" class="material-icons | open" data-page="'.$name.'.html">open_in_new</a>
				<i class="material-icons | pages__page-copy" data-page="'.$name.'.html">content_copy</i>
				<i class="material-icons | pages__page-remove" data-page="'.$name.'.html">clear</i>
			</span>
		</div>
	</li>
	';
}

listFolder($directory, ['html'], 'listPages');
?>
</ul>

<ul class="ul--list ul--dropdown tabs__content" data-tab-content="2">
	<li><div>Elements</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listElements');
	?>	
		</ul>
	</li>
	
	<li><div>Home</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listHome');
	?>	
		</ul>
	</li>
	
	<li><div>Blog</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listBlog');
	?>	
		</ul>
	</li>
	
	<li><div>Shop</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listShop');
	?>	
		</ul>
	</li>
	
	<li><div>Pages</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listPage');
	?>	
		</ul>
	</li>
	
	<li><div>Portfolio</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listPortfolio');
	?>	
		</ul>
	</li>
	
	<li><div>Specialty</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listSpecialty');
	?>	
		</ul>
	</li>
	
	<li><div>Landing</div>
		<ul>
	<?php
	listFolder($directory, ['html'], 'listLanding');
	?>	
		</ul>
	</li>

</ul>
