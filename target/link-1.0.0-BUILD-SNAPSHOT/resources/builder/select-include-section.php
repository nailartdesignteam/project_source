<?php
include "common.php";

$directory = 'include/';

function listSections($path, $name)
{
    global $directory;
    if ($name == 'mod-buttons' || $name == 'navigation')
        return;

    echo '
	<option data-dir="'.$name.'">'.$name.'</option>
	';
}


listFolder($directory, ['html'], listSections);

?>

