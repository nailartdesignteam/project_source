<?php
include "common.php";

$types = ['image/gif', 'image/jpeg', 'image/png'];
$tpl = "<div class='app__sidebar-folder__uploading__item'><div><img class='app__upload-image pointer  app__sidebar__hide' src='images/".$_POST['folder']."/".$_FILES['userfile']['name']."'></div><div><b>File:</b> <span>".$_FILES['userfile']['name']."</span><br><b>Folder:</b> <span>".$_POST['folder']."</span></div></div>";

$path = uploadFile($_FILES['userfile'], $types, 'images/'.$_POST['folder'].'/', $tpl);

if ($path == '') {
    echo 'nothing uploaded';
    return;
}

?>

<script>
		// Replace images
$('.app__sidebar-folder').on('click', '.app__upload-image', function(){
	
	var src = $(this).attr('src');
							
	$('.app__sidebar__media__container img').attr('src', src);
	$('.app__sidebar__tools [data-attr="alt"]').val('');
	$('.app__sidebar__tools [data-attr="src"]').val(src);
	
});
</script>
