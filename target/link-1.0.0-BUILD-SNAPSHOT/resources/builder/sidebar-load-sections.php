<?php
include "common.php";

$directory = $_POST['folder'].'/';

function listSections($path, $name)
{
    global $directory;

    echo '
	<div class="text-center" data-name="'.$name.'" data-directory="'.$directory.'">
		<img data-name="'.$name.'" data-directory="'.$directory.'" src="'.$path.'">
		<p>'.$name.'</p>
	</div>
	';
}


listFolder("builder/$directory", ['png'], 'listSections');

?>

<script>
var iframe = $(document).find('iframe');
var page = iframe.contents().find("body");
var win = document.getElementById('iframe').contentWindow || document.getElementById('iframe').contentDocument.defaultView;

$('.app__sidebar-page__elements__sections img').on('click', function(e){
	var el = $(this);
	var name = el.attr('data-name');
	var directory = el.attr('data-directory');
	CMain.ajaxLoadHtml('builder/sidebar-replace-section.php', "name="+name+"&directory="+directory, function(data) {
		var jData = $(data);
		if(data == '') { 
			notification('<b>PHP ERROR:</b> file_get_contents() not working on your host!<br><a href="https://stackoverflow.com/questions/27613432/file-get-contents-not-working" target="_blank">How to solve a problem?</a>');
			return false; 
			}
		jData.attr('data-name', name)
			.attr('data-directory', directory);
			
		if (jData.filter('section.bar').length){
				if(page.find('.bar').length) {
					page.find('.bar').replaceWith(jData);
				} else {
					if(page.find('nav').length) {
						page.find('nav').before(jData);	
					} else {
						page.prepend(jData);
					}
				}
			}
			
		if (jData.filter('nav').length){
				if(page.find('nav').length) {
					page.find('nav').replaceWith(jData);
				} else {
					if(page.find('section.bar').length) {
						page.find('.bar').after(jData);
					} else {
						page.prepend(jData);
					}
				}
			}	
			
		if (jData.filter('header, section:not(.bar)').length) {
			if (page.find('footer').length) {
				page.find('footer').before(jData);
			} else { 
				page.find('script:first').before(jData); 
				}
		}
			
		if (jData.filter('footer').length) {
			page.find('script:first').before(jData);
		}
		
			// Scroll to append el
		var offset = page.find(jData).offset();
		e.preventDefault();
		iframe.contents().find("body, html").stop()
			.animate({'scrollTop': offset.top}, 900, 'swing');
		
			// App reinit
		app.init()
		
			// Iframe plugins reinit
		if(page.find('.flexslider').length) {
			win.initSlider();
		}
		if(page.find('footer.footer--hidden').length) {
			win.addFixedFooter();
		}
		if(page.find('.parallax').length) {
			win.floatingtext(true);
		}
		if(page.find('.bg-video').length) {
			win.initBgVideo();
		}
		if(page.find('.instagram').length) {
			win.initInstagram();
		}
		if(page.find('.googlemap-api').length) {
			win.initMap();
		}
		if(page.find('.typed').length) {
			win.addTyped();
		}
		if(page.find('.countdown').length) {
			win.countdownUpdate();
		}
		if(page.find('.progress').length) {
			win.allProgress();
		}
		
			// fix link wrap column
		page.find('.row > a').each(function(){
		var a = $(this)
		if(a.children().is('[class*=col-]')) {
			var a_href = a.attr('href');
			var a_class = a.attr('class');
			var col = a.find('[class*=col-]:first-child');
			var a_inner = col.html()
			
			col.unwrap().empty().append('<a href="'+a_href+'" class="'+a_class+'">'+a_inner+'</a>');
		}
		
	})
		
	});
});
</script>