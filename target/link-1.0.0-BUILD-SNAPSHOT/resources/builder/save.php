<?php
$content = 
'<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
<title>'.$_POST['pagetitle'].'</title>
<meta name="keywords" content="'.$_POST['pagekey'].'" />
<meta name="description" content="'.$_POST['pagedesc'].'" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="'.$_POST['socimg'].'" />
<meta property="og:title" content="'.$_POST['soctitle'].'" />
<meta property="og:description" content="'.$_POST['socdesc'].'" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="css/main/style.css" type="text/css" />
<link rel="stylesheet" href="'.$_POST['stylefont'].'" type="text/css" />
<link rel="stylesheet" href="'.$_POST['style'].'" type="text/css" />
<link rel="stylesheet" href="css/user.css" type="text/css" />
<link rel="stylesheet" href="js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="js/prismjs/prism.css"  data-noprefix />
</head>  
<body id="body">
'.$_POST['page'].'
<script type="text/javascript" src="js/jquery-git.min.js"></script>
<script type="text/javascript" src="js/instafeed.js"></script>
<script type="text/javascript" src="js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="js/picker/picker.js"></script>
<script type="text/javascript" src="js/picker/picker.date.js"></script>
<script type="text/javascript" src="js/prismjs/prism.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/animate.js"></script>
<script type="text/javascript" src="js/waypoints.js"></script>
<script type="text/javascript" src="js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="js/typed.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script type="text/javascript" src="js/user.js"></script>
<script type="text/javascript" src="js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="js/jquery.mobile-git.min.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClwOmD0H7kExKj4A4oSmZ8jyxlg1X8rNM&callback=initMap"></script>
<script type="text/javascript" src="js/prismjs/prism.js"></script>
</body>

</html>
';

$filename = '../'.$_POST['pagename'];
$file= $filename;

$fp = fopen($file, "w");
fwrite($fp, $content);
fclose ($fp);

?>