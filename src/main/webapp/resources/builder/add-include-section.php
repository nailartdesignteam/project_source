<?php
$content = $_POST['content'];

$filename = '../include/'.$_POST['name'].'.html';
if (file_exists($filename)) {
	echo "Section <b>".$_POST['name']."</b> already exists!";
} else {
	$file= $filename;
	
	$fp = fopen($file, "w");
	fwrite($fp, $content);
	fclose ($fp);
	
	echo "Section <b>".$_POST['name']."</b> added!";
}

?>