<?php

include "common.php";

function echoItem($path, $name)
{
	echo '
<div class="masonry-item app__sidebar__hide">
		<img src="'.$path.'" title="" />
</div>';
}

listFolder($_POST['folder'], ['jpg','jpeg','gif','png'], echoItem);
?>

<script>
	
	// Upload
 $('#uploadform').attr('action', 'builder/upload-images.php');

	// Folder Tools
var sidebarFolder = $('.app__sidebar-folder');	
var folder = sidebarFolder.find('.app__sidebar__scrollarea')

var img = folder.find('img');
img.load(function(){
		 
	var CurImg = $(this);

	var height = CurImg.prop('naturalHeight');
	var width = CurImg.prop('naturalWidth');
	
	CurImg.parent().prepend(''
		+'<div class="masonry-thumb-size">'+height+' / '+width+'</div>'
		+'<div class="masonry-thumb-remove flex-center"><i class="material-icons">clear</i></div>'
	+'');
		
});

	// Replace images
folder.on('click', 'img', function(){
	
	var src = $(this).attr('src');
							
	$('.app__sidebar__media__container img').attr('src', src);
	$('.app__sidebar__tools [data-attr="alt"], .app__sidebar__tools [data-style="background-image"]').val('');
	$('.app__sidebar__tools [data-attr="src"], .app__sidebar__tools [data-style="background-image"]').val(src);
	
	if($('.socimg__container').is('.active')) {
		$('#socimg').attr('src', src);
		$('.socimg__container').removeClass('.active')
	}
	
});

</script>