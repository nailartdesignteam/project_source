<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="utf-8">

<head>
	<meta charset="utf-8">
	<title>Rush</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="robots" content="all" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta property="og:image" content="http://placehold.it/100x100" />
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />

	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
	<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
	<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
	<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
	<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />


	<style>
		.frame {
			width: 100%;
			max-width: 1300px;
			margin: 0px auto;
			clear: both;
			padding: 0px 50px;
			overflow: hidden
		}
		.padv70 {
    padding-bottom: 70px !important;
}
.detail_pic {
    width: 45%;
    float: left;
    margin: 0px 3% 15px 0px;
    padding-bottom: 20px;
}
.lSSlideOuter {
    overflow: hidden;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.lSSlideWrapper {
    max-width: 100%;
    overflow: hidden;
    position: relative;
}
.lSSlideOuter .lSPager.lSGallery {
    list-style: none outside none;
    padding-left: 0;
    margin: 0;
    overflow: hidden;
    transform: translate3d(0px, 0px, 0px);
    -moz-transform: translate3d(0px, 0px, 0px);
    -ms-transform: translate3d(0px, 0px, 0px);
    -webkit-transform: translate3d(0px, 0px, 0px);
    -o-transform: translate3d(0px, 0px, 0px);
    -webkit-transition-property: -webkit-transform;
    -moz-transition-property: -moz-transform;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.lSSlideOuter > .lSPager.lSGallery {
    margin-top: 2px !important;
}
.detail_basicinfo {
    margin-bottom: 20px;
    border-top: 2px solid #000000;
		width: 52%;
    float: left;
}
.detail_header, .detail_content {
    border-bottom: 1px solid #bdbdbd;
}
.detail_header, .detail_content, .detail_today_title {
    padding: 24px 20px;
}
.detail_deliver, .last_detail {
    margin-bottom: 19px;
}
.detail_price_calc {
    padding: 30px 20px;
		font-size: 1.4em;
    text-align: right;
    font-weight: bold;
}
.detail_price_calc span {
    float: left;
    font-size: 0.8em;
    color: #bdbdbd;
    font-weight: normal;
}
.detail_price_calc span{
    line-height: 26px;
}
.detail_price_calc b {
    color: #ff4c42;
    margin-left: 10px;
}
.detail_btns {
    padding: 0 15px;
}
#btn_cart {
    width: 100%;
    background: #727272;
}
.detail_btns a{
    border-radius: 4px;
}
.detail_btns a {
    float: left;
    display: block;
    height: 60px;
    line-height: 60px;
    border: 1px solid #ffffff;
    color: #ffffff;
    background: #000000;
    text-align: center;
    font-weight: bold;
}
.detail_title {
    font-size: 25px;
    padding: 0;
    border-top: none;
}
.detail_price {
    margin-top: 8px;
    font-size: 24px;
}
.detail_price {
    padding: 0;
}
.detail_price_calc {
    padding: 20px;
    font-size: 1.4em;
    text-align: right;
    font-weight: bold;
}
.btns_orderinfo_gray {text-align: right; overflow: hidden; border-bottom: 1px solid #cccccc}
.mypage_order_item_wrap { margin-bottom: 100px }
.mypage_order_item_wrap h2 {font-weight: 200; font-size: 1.8em; border-bottom: 1px solid #000; padding-bottom: 16px;}
.mypage_order_item_wrap h2 span:first-child {border-right: solid 1px #000000; margin: 0 10px; padding-top: 16px; font-size: 0; line-height: 0;}

.order_item { display: table; width:100%; }
.order_item dl { width: 100%; display: table-row; }

.order_item dd, .order_item dt {display: table-cell; vertical-align: middle; padding: 10px}
.order_item dl dt {padding-bottom: 10px; font-size: 1.3em; font-weight: bold; text-align: center}
.order_item dl dt:first-child {text-align: left}
.order_item dt img {width: 100%; max-width: 140px}
.order_item dl dd {font-size: 1.25em}
.order_item dl dd:first-child {width : 15%}
.order_item dl dd:nth-child(3) {width : 20%; text-align: center}
.order_item dl dd:nth-child(4) {width : 15%; text-align: center}
.order_item dd:first-child img {width: 100%; max-width: 140px}
.connect_review_img {
	float: left;
	display: inline-block;
	width: 120px;
	height: 120px;
	vertical-align: middle;
	margin-right: 70px;
}
.connect_review_info {
	float: left;
	line-height: 18px;
	padding-top: 8px;
}
.connect_review_info .txt_brand {
	margin-bottom: 4px;
	font-weight: bold;
}
.btns_orderinfo_gray {text-align: right; overflow: hidden; border-bottom: 2px solid #cccccc}
	</style>

<!-- 여기 -->



</head>

<body id="body">

<!-- 여기 -->
	<%-- 
	<%@ include file="../common/header.jsp" %>
	 --%>
	 
	<section class="bg-color--white-2 padding-null bar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 hidden-xs">
					<span>Rush – HTML5 Template with Page Builder</span>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right text-left-xs">
					<a href="mail:">arvel.purdy@volkman.name</a>
					<a href="tel:" class="text--nowrap">725-637-5464</a>
					<a href="#" class="btn btn--line btn-xs right-xs">button</a>
				</div>
			</div>
		</div>
	</section>
	<nav class="hr bg-color--white-2">
		<div class="container">
			<div class="row row--nav row--nav--baseline">
				<div class="nav-mod logo left">
					<a href="index.html"><img alt="" src="resources/images/logo--black.png" height="22"></a>
				</div>
				<div class="nav-mod nav-toggle right visible-xs visible-sm">
					<i class="material-icons pointer">&#xE5D2;</i>
				</div>
				<div class="nav-mod nav hidden-xs hidden-sm left">
					<ul class="menu text--uppercase">
						<div data-include="navigation"></div>
					</ul>
				</div>
				<div class="nav-mod-group right left-xs left-sm hidden-xs">
					<div class="search__content boxed--shadow hr">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<form class="form--radius-xs">
										<div class="form-inline form-inline-xs">
											<input type="search" placeholder="Type and hit enter to search">
											<i class="material-icons search__content__close">&#xE14C;</i>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="nav-mod search hidden-xs">
						<i class="material-icons">&#xE8B6;</i>
					</div>
					<div class="nav-mod  dropdown dropdown--right dropdown--toggle-on-click dropdown--left-sm">
						<i class="material-icons dropdown__toggle">&#xE8CC;</i>
						<div class="dropdown__content dropdown__content--2 bg-color--white">
							<div class="row hr">
								<div class="col-md-3 col-sm-3 col-xs-3 text-center">
									<img alt="" src="resources/images/items/thumb-item%234.png" height="40">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<b class="text--uppercase">Bag<span class="right">4</span></b>
									<p class="p-xs marg-null">Gray, Orange</p>
								</div>
								<div class="col-md-1 col-sm-1 col-xs-1 text-right">
									<i class="material-icons ico-xs">&#xE14C;</i>
								</div>
							</div>
							<div class="row hr">
								<div class="col-md-3 col-sm-3 col-xs-3 text-center">
									<img alt="" src="resources/images/items/thumb-item%237.png" height="40">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<b class="text--uppercase">Sport bag<span class="right">1</span></b>
									<p class="p-xs marg-null">Black, Orange</p>
								</div>
								<div class="col-md-1 col-sm-1 col-xs-1 text-right">
									<i class="material-icons ico-xs">&#xE14C;</i>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6">
									<a href="#" class="btn btn-md btn--line">Cart</a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<b class="price__label">CART TOTAL</b><br>
									<span class="price price-xs">2.000$</span>
								</div>
							</div>
						</div>
					</div>
					<div data-include="mod-buttons"></div>
				</div>
			</div>
		</div>
	</nav>
	<section class="boxed--shadow padding-null">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 boxed-xs marg-null">
					<ul class="ul--list ul--inline ul-xs">
						<li class="menu__dropdown__title">
							<h4>Products</h4>
						</li>
						<li>
							<a href="#">Studio</a>
						</li>
						<li>
							<a href="#">Travel</a>
						</li>
						<li>
							<a href="#">Company</a>
						</li>
						<li>
							<a href="#">Company 2</a>
						</li>
						<li>
							<a href="#">Company 3</a>
						</li>
						<li>
							<a href="#"><i class="material-icons"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

<div class="detail_info frame padv70" style="padding-top:80px;">
	<div class="detail_pic">
		<div class="lSSlideOuter  noPager">
			<div class="lSSlideWrapper usingCss">
				<ul id="imageGallery" class="lightSlider lSSlide" style="width: 471px; transform: translate3d(0px, 0px, 0px); height: 511px; padding-bottom: 0%;">
					<li data-thumb="https://image.brandi.me/cproduct/2019/05/24/8641498_1558685872_image1_L.jpg" class="lslide active" style="width: 471px; margin-right: 0px;">
						<img src="https://image.brandi.me/cproduct/2019/05/24/8641498_1558685872_image1_L.jpg" style="width: 100%;">
					</li>
				</ul>
				<div class="lSAction" style="display: none;">
					<a class="lSPrev"></a>
					<a class="lSNext"></a>
				</div>
			</div>
			<ul class="lSPager lSGallery" style="margin-top: 5px; transition-duration: 400ms; width: 95.7px; transform: translate3d(0px, 0px, 0px);">
			</ul>
		</div>
	</div>
	<div class="detail_basicinfo">
		<div class="detail_header">
			<div class="detail_title">${nail.nailShopName}</div>
			<div class="detail_price">${nail.name}
			</div>
		</div>
		<div class="detail_content">
			<div class="detail_deliver">${nail.content}
			</div>
	</div>
	<div class="detail_footer">
		<div class="detail_price_calc">
			총 금액<b style="font-size: 1.4em;">${nail.price}</b>
		</div>
	</div>
	<div class="detail_btns">
		<a href="${pageContext.request.contextPath }/myWishList/add?nailId=${nail.id}&userId=duswo" id="btn_cart" class="m btn_cart">장바구니 담기</a>
	</div>
</div>
</div>

<section style="padding-top:0px;">
		<div class="container tabs">
				<div class="">

				</div>
				<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
								<ul class="tabs__links hr">
										<li data-tab-link="1" class="active">상세정보</li>
										<li data-tab-link="2">리뷰</li>
								</ul>
						</div>
				</div>
				<div class="tabs__content active" data-tab-content="1">

				</div>
				<div class="tabs__content" data-tab-content="2">
					<div class="mypage_order_item_wrap frame">
					<c:forEach var="review" items="${reviews}">
						<h2>${review.userId}<span></span>${review.date}</h2>
								<div class="order_item order_item_no_btn">
									<dl style="display: block;">
										<dd class="review_text" style="padding: 10px;
				    clear: both;">${review.content}</dd>
										<dd class="review_thumb" style="display: block;">
											<a href="https://image.brandi.me/media/2019/05/22/1001261_1558532656_image1_L.jpg" class="fancybox">
												<img src="resources/images/yeonjae/${review.image}"></a>
										</dd>
									</dl>
									</div>
									<div class="btns_orderinfo_gray"></div>
									</c:forEach>
								</div>
				</div>
		</div>
</section>

	<footer class="bg-color--white-2">
	</footer>



	<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
	<script type="text/javascript" src="resources/js/instafeed.js"></script>
	<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="resources/js/picker/picker.js"></script>
	<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
	<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
	<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/animate.js"></script>
	<script type="text/javascript" src="resources/js/waypoints.js"></script>
	<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
	<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
	<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="resources/js/typed.js"></script>
	<script type="text/javascript" src="resources/js/main.js"></script>
	<script type="text/javascript" src="resources/js/user.js"></script>
	<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
	<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
	<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>
