<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>


<!DOCTYPE html>
<html lang="UTF-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/prismjs/prism.css"  data-noprefix />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/jquery.timepicker.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/prismjs/prism.css"  data-noprefix />
<link rel="stylesheet" href="${pageContext.request.contextPath}/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery/jquery-ui.css?version=1.3"  type="text/css" media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

</head>  
<body>
   <section class="bg-color--white padding-null">
      <div class="container-fluid visible-xs">
         <div class="row row--fluid">
            <div class="col-md-12 col-sm-12 col-xs-12" data-shadow-top="8">
               <img src="images/hi/nailmain2.jpg" alt="">
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row marg-t-60 marg-b-60">
            <div class="col-md-6 col-sm-6 col-xs-12">
               <c:forEach var="shop" items="${shop }">
               <h1 class="h2">${shop.name }</h1>
               </c:forEach>
            
               <p class="p-lg">예약 날짜 : ${reservation.date }</p>
               <p class="p-lg">예약 시간 : ${reservation.time }</p>
               <p class="p-lg">디자이너  : ${reservation.designer }</p>
                              
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 hidden-xs">
            <c:forEach var="nail" items="${nail}">
               <img src="${pageContext.request.contextPath }/resources/images/yong/${nail.image }" alt="">
               <p class="p-lg">예약네일명  : ${nail.name }</p>
            </c:forEach>
            </div>
         </div>
      </div>
   </section>
</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-git.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/instafeed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/countdown-master/dest/jquery.countdown.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.date.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prismjs/prism.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/waypoints.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mb.YTPlayer.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/masonry.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/imagesloaded.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/typed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/user.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/placeholder/placeholder.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mobile-git.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.timepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js//jquery-1.8.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/jquery/jquery-ui.js?version=1.3"/>"></script>
<script type="text/javascript" src="<c:url value="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"/>"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link href="${pageContext.servletContext.contextPath}/resources/jquery/jquery-ui.css?version=1.3" rel="stylesheet" type="text/css" media="screen">
<script src="${pageContext.servletContext.contextPath}/resources/js//jquery-1.8.3.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/resources/jquery/jquery-ui.js?version=1.3"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

</html>