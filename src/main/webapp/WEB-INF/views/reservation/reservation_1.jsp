<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/jquery.timepicker.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/prismjs/prism.css"  data-noprefix />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery/jquery-ui.css?version=1.3"  type="text/css" media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/jquery.timepicker.min.css">
<%-- <link rel="style"sheet" href="${pageContext.request.contextPath}/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.timepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.structure.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.theme.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.theme.min.css">


<body id="body">
	<%@ include file="../common/header.jsp" %>
	<header class="vh-70 background-image overflow-visible flex flex-wrap flex-vertical" data-opacity="2">
		<div class="bg" data-shadow-bottom="3" style="background: url('${pageContext.request.contextPath}/resources/images/hi/nailmain2.jpg');"></div>
		<div class="container flex-vertical__top marg-t-60">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					
				</div>
			</div>
		</div>
		<div class="container flex-vertical__center marg-t-60 marg-b-60">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<h1 class="h3">Build your pages with Rush template. It’s easy!</h1>
					<p class="p-lg">What Makes Flyers Unrivaled</p>
				</div>
			</div>
		</div>
		<div class="container flex-vertical__bottom">
			<div class="row marg-b-60">
				<div class="col-md-12 col-sm-12 col-xs-12 ">
					<form action="search" method="GET">
						<div class="row input-group form-inline marg-null">
							<div class="col-md-5 col-sm-12 col-xs-12 ">
								<label>예약 날짜</label>
								
								<input type="text" id="datepicker" name="date" placeholder="date" readonly="readonly">
					     <%--     	<input class="input-date picker__input" type="date" placeholder="Departure" readonly="" id="datepicker" aria-haspopup="true" aria-expanded="false" aria-readonly="false" aria-owns="P1501753302_root" name="date" ><div class="picker" id="P1501753302_root"  aria-hidden="true"><div class="picker__holder" tabindex="-1"><div class="picker__frame"><div class="picker__wrap"><div class="picker__box"><div class="picker__header"><div class="picker__month">May</div><div class="picker__year">2019</div><div class="picker__nav--prev" data-nav="-1" role="button" aria-controls="P1501753302_table" title="Previous month"> </div><div class="picker__nav--next" data-nav="1" role="button" aria-controls="P1501753302_table" title="Next month"> </div></div><table class="picker__table" id="P1501753302_table" role="grid" aria-controls="P1501753302" aria-readonly="true"><thead><tr><th class="picker__weekday" scope="col" title="Sunday">Sun</th><th class="picker__weekday" scope="col" title="Monday">Mon</th><th class="picker__weekday" scope="col" title="Tuesday">Tue</th><th class="picker__weekday" scope="col" title="Wednesday">Wed</th><th class="picker__weekday" scope="col" title="Thursday">Thu</th><th class="picker__weekday" scope="col" title="Friday">Fri</th><th class="picker__weekday" scope="col" title="Saturday">Sat</th></tr></thead><tbody><tr><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1556377200000" role="gridcell" aria-label="28 April, 2019">28</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1556463600000" role="gridcell" aria-label="29 April, 2019">29</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1556550000000" role="gridcell" aria-label="30 April, 2019">30</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1556636400000" role="gridcell" aria-label="1 May, 2019">1</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1556722800000" role="gridcell" aria-label="2 May, 2019">2</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1556809200000" role="gridcell" aria-label="3 May, 2019">3</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1556895600000" role="gridcell" aria-label="4 May, 2019">4</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1556982000000" role="gridcell" aria-label="5 May, 2019">5</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557068400000" role="gridcell" aria-label="6 May, 2019">6</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557154800000" role="gridcell" aria-label="7 May, 2019">7</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557241200000" role="gridcell" aria-label="8 May, 2019">8</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557327600000" role="gridcell" aria-label="9 May, 2019">9</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557414000000" role="gridcell" aria-label="10 May, 2019">10</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557500400000" role="gridcell" aria-label="11 May, 2019">11</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557586800000" role="gridcell" aria-label="12 May, 2019">12</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557673200000" role="gridcell" aria-label="13 May, 2019">13</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557759600000" role="gridcell" aria-label="14 May, 2019">14</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557846000000" role="gridcell" aria-label="15 May, 2019">15</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1557932400000" role="gridcell" aria-label="16 May, 2019">16</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558018800000" role="gridcell" aria-label="17 May, 2019">17</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558105200000" role="gridcell" aria-label="18 May, 2019">18</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558191600000" role="gridcell" aria-label="19 May, 2019">19</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558278000000" role="gridcell" aria-label="20 May, 2019">20</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558364400000" role="gridcell" aria-label="21 May, 2019">21</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558450800000" role="gridcell" aria-label="22 May, 2019">22</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558537200000" role="gridcell" aria-label="23 May, 2019">23</div></td><td role="presentation"><div class="picker__day picker__day--infocus picker__day--today picker__day--highlighted" data-pick="1558623600000" role="gridcell" aria-label="24 May, 2019" aria-activedescendant="true">24</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558710000000" role="gridcell" aria-label="25 May, 2019">25</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558796400000" role="gridcell" aria-label="26 May, 2019">26</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558882800000" role="gridcell" aria-label="27 May, 2019">27</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1558969200000" role="gridcell" aria-label="28 May, 2019">28</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1559055600000" role="gridcell" aria-label="29 May, 2019">29</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1559142000000" role="gridcell" aria-label="30 May, 2019">30</div></td><td role="presentation"><div class="picker__day picker__day--infocus" data-pick="1559228400000" role="gridcell" aria-label="31 May, 2019">31</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559314800000" role="gridcell" aria-label="1 June, 2019">1</div></td></tr><tr><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559401200000" role="gridcell" aria-label="2 June, 2019">2</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559487600000" role="gridcell" aria-label="3 June, 2019">3</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559574000000" role="gridcell" aria-label="4 June, 2019">4</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559660400000" role="gridcell" aria-label="5 June, 2019">5</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559746800000" role="gridcell" aria-label="6 June, 2019">6</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559833200000" role="gridcell" aria-label="7 June, 2019">7</div></td><td role="presentation"><div class="picker__day picker__day--outfocus" data-pick="1559919600000" role="gridcell" aria-label="8 June, 2019">8</div></td></tr></tbody></table><div class="picker__footer"><button class="picker__button--today" type="button" data-pick="1558623600000" aria-controls="P1501753302" disabled="disabled">Today</button><button class="picker__button--clear" type="button" data-clear="1" aria-controls="P1501753302" disabled="disabled">Clear</button><button class="picker__button--close" type="button" data-close="true" aria-controls="P1501753302" disabled="disabled">Close</button></div></div></div></div></div></div>
								--%>
								<%--	<div class="input__helper">
										<i class="material-icons ico-xs"></i>
									</div>   --%>
								</div>
								<div class="col-md-5 col-sm-12 col-xs-12">
								<label>시간</label>
								<input type="text" placeholder="time" id="timepicker"  name="time" >
								
							</div>
							
						</div>
						    <div class="row marg-b-60">
							  <div class="col-md-2 col-md-offset-5 col-sm-3 col-sm-offset-5 col-xs-12">
								<input type="submit" class="btn btn--default btn--stretched" value="Search">
							  </div>
						    </div>
							
						
					</form>
				</div>
			</div>
		</div>
	</header>
	<section class="bg-color--white-2">
<!-- 	 <form action="select" method=""> -->
		<div class="container">
			<div class="row flex flex-wrap flex-equal">
				<div class="col-md-4 col-sm-6 col-xs-12">
				
				  <c:forEach items="${shops }" var="shop">	
				   <a href="${pageContext.request.contextPath }/select?shopId=${shop.id}" >
					<div class="boxed--radius-sm boxed--shadow ">
						<div class="container"></div>
						<div class="boxed">
							<h5>${shop.name}</h5>
						</div>
					</div>
					</a>
				  </c:forEach>
				 
				</div>
				</div>
			</div>
	<!-- 	
		<div class="container">
			<div class="row marg-t-60">
				<div class="col-md-2 col-md-offset-5 col-sm-3 col-sm-offset-5 col-xs-12">
						<input type="submit" class="btn btn--default btn--stretched" value="select">
				</div>	
			</div>
		</div> -->
	</section>



</body>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-git.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/instafeed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/countdown-master/dest/jquery.countdown.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.date.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prismjs/prism.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/waypoints.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mb.YTPlayer.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/masonry.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/imagesloaded.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/typed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/user.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/placeholder/placeholder.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mobile-git.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.timepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js//jquery-1.8.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-ui.js?version=1.3"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/external/jquery/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>  
<script type="text/javascript" src="<c:url value="/resources/js/timepicker/jquery.timepicker.min.js"/>"></script><!-- 타이머js -->
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/timepicker/jquery.timepicker.css" media=""/><!-- 타이머css -->


</head>  

<script>
		$(function(){
			$( "#datepicker" ).datepicker({
				/* showOn: "button", */
				/* inline: true, */
				showOtherMonths: true,
				showButtonPanel: true, 
		        currentText: '오늘 날짜', 
		        closeText: '닫기', 
		        dateFormat: "yy-mm-dd",
		        changeMonth: true,
		        dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],
				dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
				minDate: 0
			});
			$( "#timepicker" ).timepicker({
							
				
				timeFormat: "h:mm p",
				interval: 60,
				minTime: '10',
				maxTime: '6:00pm',
				defaultTime: '11',
				startTime: '10:00',
				dynamic: false,
				dropdown: true,
				scrollbar: true
			});
		});	
		
		
			
		

</script>


</html>

