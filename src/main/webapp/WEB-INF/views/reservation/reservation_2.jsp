<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" isELIgnored="false"%>
<!DOCTYPE html>
<html>

<head>

<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="robots" content="all">

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100">
<meta property="og:title" content="">
<meta property="og:description" content="">

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/prismjs/prism.css"  data-noprefix />


</head>

<body id="body">   
   
   <nav class="bg-color--white">
      <div class="container">
         <div class="row row--nav row--nav--baseline" style="line-height: 72px;">
            <div class="nav-mod logo left">
               <a href="index.html"><img alt="" src="${pageContext.request.contextPath}/resources/images/logo--black.png" height="22"></a>
            </div>
            <div class="nav-mod nav-toggle right visible-xs visible-sm">
               <i class="material-icons pointer"></i>
            </div>
            <div class="nav-mod nav hidden-xs hidden-sm left">
               <ul class="menu text--uppercase">
                  
               </ul>
            </div>
            <div class="nav-mod-group right left-xs left-sm hidden-xs">
               <div data-include="mod-buttons"></div>
            </div>
         </div>
      </div>
   </nav>
   
   <section class="bg-color--white-2 padding-null">
      <div class="container">
         <div class="row flex flex-wrap flex-baseline marg-t-120">
            <div class="col-md-6 col-sm-6 col-xs-12">
            <c:forEach var="shop" items="${shop}">
                <h1 class="h2">${shop.name}</h1>
            </c:forEach>
            </div>
         <%--   <div class="col-md-6 col-sm-6 col-xs-12 text-right">
               <ul class="menu ul--inline text--uppercase">
                  <li><a href="#">Photography</a></li>
                  <li><a href="#">Company</a></li>
                  <li><a href="#">Studio</a></li>
                  <li><a href="#">Products</a></li>
               </ul>
            </div>  --%>
         </div>
      </div>
   </section>
   
   <section class="bg-color--white-2">
    <div class="container">
    <div class="row grid gallery">
    <c:forEach items="${nails}" var="nail">
            <a href="${pageContext.request.contextPath }/image?nailId=${nail.id}&shopId=${nail.nailShopName}&userId=${userId}">
            <div class="col-md-4 col-sm-4 col-xs-6 grid-item grid-sizer">
               <img src="${pageContext.request.contextPath }/resources/images/yong/${nail.image}" alt="">
               <p>${nail.name}</p>
            </div>
            </a>
   </c:forEach>
      <%-- <div class="row flex flex-wrap flex-equal">
         <div class="col-md-4 col-sm-6 col-xs-12">
           <a href="${pageContext.request.contextPath }/image?id=${nail.nailShopName}">
            <c:forEach items="${nails}" var="nail">
              <div class="background-image hover h-3 boxed--shadow boxed--radius">
                  <div class="col-md-4 col-sm-4 col-xs-6 grid-item">
                   <div class="bg" style="background: url('${pageContext.request.contextPath }/resources/images/yong/${nail.image}');">
                   </div>
                  </div>
                  <div class="hover__animate" data-shadow-bottom="8"></div>
                  <div class="container pos-absolute pos-bottom">
                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <div class="boxed hover__animate  hover__animate--slide-in">
                              
                              <p>${nail.name}</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </c:forEach>
         </a>   
         </div>
      </div> --%>
   </div>
   </div>   
   </section>
      
   

</body>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-git.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/instafeed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/countdown-master/dest/jquery.countdown.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/picker/picker.date.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/prismjs/prism.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/animate.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/waypoints.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mb.YTPlayer.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/masonry.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/imagesloaded.pkgd.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/typed.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/user.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/placeholder/placeholder.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.mobile-git.min.js"/>"></script>




</html>