<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css">
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

</head>
<body id="body">
	<style>
	.frame {width:100%; max-width: 1300px; margin: 0px auto; clear: both; padding:0px 50px; overflow: hidden}

	.btns_orderinfo_gray {text-align: right; overflow: hidden; border-bottom: 1px solid #cccccc}
	.mypage_order_item_wrap { margin-bottom: 100px }
	.mypage_order_item_wrap h2 {font-weight: 200; font-size: 1.8em; border-bottom: 1px solid #000; padding-bottom: 16px;}
	.mypage_order_item_wrap h2 span:first-child {border-right: solid 1px #000000; margin: 0 10px; padding-top: 16px; font-size: 0; line-height: 0;}

	.order_item { display: table; width:100%; }
	.order_item dl { width: 100%; display: table-row; }

	.order_item dd, .order_item dt {display: table-cell; vertical-align: middle; padding: 10px}
	.order_item dl dt {padding-bottom: 10px; font-size: 1.3em; font-weight: bold; text-align: center}
	.order_item dl dt:first-child {text-align: left}
	.order_item dt img {width: 100%; max-width: 140px}
	.order_item dl dd {font-size: 1.25em}
	.order_item dl dd:first-child {width : 15%}
	.order_item dl dd:nth-child(3) {width : 20%; text-align: center}
	.order_item dl dd:nth-child(4) {width : 15%; text-align: center}
	.order_item dd:first-child img {width: 100%; max-width: 140px}
	.connect_review_img {
    float: left;
    display: inline-block;
    width: 120px;
    height: 120px;
    vertical-align: middle;
    margin-right: 70px;
}
.connect_review_info {
    float: left;
    line-height: 18px;
    padding-top: 8px;
}
.connect_review_info .txt_brand {
    margin-bottom: 4px;
    font-weight: bold;
}
.btns_orderinfo_gray {text-align: right; overflow: hidden; border-bottom: 2px solid #cccccc}
	</style>

	<%@ include file="../common/header.jsp" %>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 marg-null">
					<ul class="breadcrumbs">
						<li><a href="#">마이페이지</a></li>
						<li><span>마이 리뷰</span></li>
					</ul>
				</div>
			</div>
		</div>
	</section>


	<div class="mypage_order_item_wrap frame">
				<c:forEach items="${reviews}" var="reviews">
		<h2>${reviews.id} | ${reviews.date} | 작성자 : ${reviews.userId}</h2>
				
				<div class="order_item order_item_no_btn">
				
					<dl>
						<dd class="reviewer_info" style="border-bottom: 1px solid #bdbdbd;line-height: 180%; width:100%; padding">
							<div class="connect_review_img">
								<a href="/app/product/detail/644037/0">
									<img src="" alt="Nail Design Image">
								</a>
							</div>
							
							<c:forEach items="${shops}" var="shops">
							<div class="connect_review_info">
								<p class="txt_brand">${shops.name}<!--[Nail Shop Name]-->
								</p>
								</div>
						   </c:forEach>
								<p class="list_info p_name">
									<a href="/app/product/detail/644037/0">Nail Design Name</a>
								</p>
							
							
						</dd>
				   </dl>
				  
					<dl style="display: block;">
						<dd class="review_text" style="padding: 10px; clear: both;">${reviews.content}</dd>
						<dd class="review_thumb" style="display: block;">
							<a href="https://image.brandi.me/media/2019/05/22/1001261_1558532656_image1_L.jpg" class="fancybox">
								<img src="https://image.brandi.me/media/2019/05/22/1001261_1558532656_image1_S.jpg"></a>
						</dd>
					</dl>
					
					</div>
					<div class="btns_orderinfo_gray"></div>
				</c:forEach>
				</div>
	<footer class="bg-color--white-2">
	</footer>



<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>
