<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="ko">

<head>
	<meta charset="utf-8">
	<title>Rush</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="robots" content="all" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta property="og:image" content="http://placehold.it/100x100" />
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />

	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
	<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css" />
	<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css">
	<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
	<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
	<link rel="stylesheet" href="resources/js/prismjs/prism.css" data-noprefix />

</head>

<body id="body">
	<style>
		.frame {
			width: 100%;
			max-width: 1300px;
			margin: 0px auto;
			clear: both;
			padding: 0px 300px;
			overflow: hidden
		}
		.pagetitle {
		    text-align: center;
		    padding: 40px 0px 20px 0px;
		    clear: both;
				    padding-bottom: 0;
		}
		.pagetitle h1 {
    font-size: 2.2em;
    font-weight: normal;
    text-align: center;
    margin-bottom: 15px;
}
.mypage_iconbox {
    display: block;
    padding: 60px 0;
}
.mypage_iconbox {
    background: #fcfcfc;
    border: 1px solid #000000;
    overflow: hidden;
}
.mypage_iconbox li {
    position: relative;
}
.mypage_iconbox li {
		width: 33%;
    display: block;
    float: left;
    text-align: center;
    border-right: 1px solid #000;
}
.mypage_iconbox .on {
    font-weight: 700;
    color: #000000;
}
.mypage_iconbox li a {
    color: #7d7d7d;
    font-size: 1.1em;
}
.mypage_iconbox .on img {
    opacity: 1;
}
.mypage_iconbox span {
    display: inline-block;
    line-height: 18px;
    vertical-align: top;
    font-size: 1.1em;
}
.mypage_iconbox img.mypage_nav_arrow {
    width: 12px;
}
.mypage_iconbox li:last-child {
    border-right: none;
}

	</style>

	<section class="bg-color--white-2 padding-null bar">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 hidden-xs">
					<span>Rush – HTML5 Template with Page Builder</span>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 text-right text-left-xs">
					<a href="mail:">arvel.purdy@volkman.name</a>
					<a href="tel:" class="text--nowrap">725-637-5464</a>
					<a href="#" class="btn btn--line btn-xs right-xs">button</a>
				</div>
			</div>
		</div>
	</section>
	<nav class="hr bg-color--white-2">
		<div class="container">
			<div class="row row--nav row--nav--baseline">
				<div class="nav-mod logo left">
					<a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
				</div>
				<div class="nav-mod nav-toggle right visible-xs visible-sm">
					<i class="material-icons pointer">&#xE5D2;</i>
				</div>
				<div class="nav-mod nav hidden-xs hidden-sm left">
					<ul class="menu text--uppercase">
						<div data-include="navigation"></div>
					</ul>
				</div>
				<div class="nav-mod-group right left-xs left-sm hidden-xs">
					<div class="search__content boxed--shadow hr">
						<div class="container">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<form class="form--radius-xs">
										<div class="form-inline form-inline-xs">
											<input type="search" placeholder="Type and hit enter to search">
											<i class="material-icons search__content__close">&#xE14C;</i>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="nav-mod search hidden-xs">
						<i class="material-icons">&#xE8B6;</i>
					</div>
					<div class="nav-mod  dropdown dropdown--right dropdown--toggle-on-click dropdown--left-sm">
						<i class="material-icons dropdown__toggle">&#xE8CC;</i>
						<div class="dropdown__content dropdown__content--2 bg-color--white">
							<div class="row hr">
								<div class="col-md-3 col-sm-3 col-xs-3 text-center">
									<img alt="" src="images/items/thumb-item%234.png" height="40">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<b class="text--uppercase">Bag<span class="right">4</span></b>
									<p class="p-xs marg-null">Gray, Orange</p>
								</div>
								<div class="col-md-1 col-sm-1 col-xs-1 text-right">
									<i class="material-icons ico-xs">&#xE14C;</i>
								</div>
							</div>
							<div class="row hr">
								<div class="col-md-3 col-sm-3 col-xs-3 text-center">
									<img alt="" src="images/items/thumb-item%237.png" height="40">
								</div>
								<div class="col-md-8 col-sm-8 col-xs-8">
									<b class="text--uppercase">Sport bag<span class="right">1</span></b>
									<p class="p-xs marg-null">Black, Orange</p>
								</div>
								<div class="col-md-1 col-sm-1 col-xs-1 text-right">
									<i class="material-icons ico-xs">&#xE14C;</i>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-6">
									<a href="#" class="btn btn-md btn--line">Cart</a>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<b class="price__label">CART TOTAL</b><br>
									<span class="price price-xs">2.000$</span>
								</div>
							</div>
						</div>
					</div>
					<div data-include="mod-buttons"></div>
				</div>
			</div>
		</div>
	</nav>
	<section class="boxed--shadow padding-null">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 boxed-xs marg-null">
					<ul class="ul--list ul--inline ul-xs">
						<li class="menu__dropdown__title">
							<h4>Products</h4>
						</li>
						<li>
							<a href="#">Studio</a>
						</li>
						<li>
							<a href="#">Travel</a>
						</li>
						<li>
							<a href="#">Company</a>
						</li>
						<li>
							<a href="#">Company 2</a>
						</li>
						<li>
							<a href="#">Company 3</a>
						</li>
						<li>
							<a href="#"><i class="material-icons"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

<div class="pagetitle"><h1>마이페이지</h1></div>
<div class="frame">
<div class="mypage_iconbox">

	<ul>
		<li><a href="${pageContext.request.contextPath}/myReservation?shopId=${shopId}&userId=${userId}" class="router-link-exact-active router-link-active on">
			<img src="/resources/images/seonyoung/reservation_icon.png"> <br>
			<span>예약 조회</span></a>
		</li>
		<li><a href="${pageContext.request.contextPath}/myReview?userId=${userId}&shopId=${shopId}" class="">
			<img src="/resources/images/seonyoung/review_icon.png"> <br>
			<span>마이 리뷰</span></a>
		</li>
		
		<li><a href="${pageContext.request.contextPath}/myCart" class="">
			<img src="/resources/images/seonyoung/cart_icon.png"> <br>
			<span>장바구니</span></a>
		</li>
	</ul>

</div>
</div>

	<footer class="bg-color--white-2">
	</footer>



	<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
	<script type="text/javascript" src="resources/js/instafeed.js"></script>
	<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="resources/js/picker/picker.js"></script>
	<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
	<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
	<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="resources/js/animate.js"></script>
	<script type="text/javascript" src="resources/js/waypoints.js"></script>
	<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
	<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
	<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
	<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
	<script type="text/javascript" src="resources/js/typed.js"></script>
	<script type="text/javascript" src="resources/js/main.js"></script>
	<script type="text/javascript" src="resources/js/user.js"></script>
	<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
	<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
	<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>
