<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
		
		<!-- ajax lib -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
		
	</head>
	<body>
	
		<div id="append_article"></div>
		<a href="list?page=${page+10}">다음페이지</a>
		<a href="#" onclick="loadNextPage('${page+10}')">다음페이지</a>
	</body>
	
	<script>
		function loadNextPage(page){
			var param = "page="+page;
			$('#append_article').load("list", param, function(data){
			alert(data);
			});
		}
	</script>
</html>