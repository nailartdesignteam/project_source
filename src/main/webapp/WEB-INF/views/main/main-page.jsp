<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

<!-- 재윤재윤 -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery/jquery-ui.css?version=1.3"  type="text/css" media="screen">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/js/picker/themes/jquery.timepicker.min.css">
<%-- <link rel="style"sheet" href="${pageContext.request.contextPath}/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css"> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery.timepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.structure.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.structure.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.theme.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.theme.min.css">

<style>
   .imgAlbum {
      width: 80px;
      height: 80px;
      margin-right: 10px;
   }
   
   .inline-yong{
      display: inline;
      float: left;
   }
   
   .inline-test{
      display:inline-block;
   }
   
</style>

</head>  

<body id="body">   
   
   
   <%@ include file="../common/header.jsp" %>


   <header class="vh-70 background-image overflow-visible flex flex-wrap flex-vertical" data-opacity="2">
      <div class="bg" data-shadow-bottom="3" style="background: url('resources/images/hi/nailmain2.jpg');"></div>
      <div class="container flex-vertical__top marg-t-60">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               
            </div>
         </div>
      </div>
      <div class="container flex-vertical__center marg-t-60 marg-b-60">
         <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
               <h1 class="h3">Build your pages with Rush template. It’s easy!</h1>
               <p class="p-lg">What Makes Flyers Unrivaled</p>
            </div>
         </div>
      </div>
      <div class="container flex-vertical__bottom">
         <div class="row marg-b-60">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <form action="mainSearch" method="">
                  <div class="row input-group form-inline marg-null">
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <label>네일샵</label>
                        <div class="input">
                           <input type="text" placeholder="네일샵" name="shop">
                       
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <label>디자이너</label>
                        <div class="input">
                           <input type="text" placeholder="Designer" name="designer">
                        </div>
                     </div>
                     <div class="col-md-3 col-sm-5 col-xs-12">
                        <label>예약 날짜</label>
                        
                  <input type="text" id="datepicker" name="date" placeholder="date" readonly="readonly">
                     </div>
                     <div class="col-md-3 col-sm-5 col-xs-12">
                        <label>시간</label>
                  <input type="text" placeholder="time" id="timepicker"  name="time" >
                        
                     </div>
                  </div>
                  <div class="row marg-b-60">
                     <div class="col-md-2 col-md-offset-5 col-sm-3 col-sm-offset-5 col-xs-12">
                        <input type="submit" class="btn btn--default btn--stretched" value="Search">
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </header>
   
  
   

   <section class="bg-color--white-2">
      <div class="container">
         <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8">
               <h4>디자인</h4>
               <i class="material-icons swipeleft pointer">&#xE5C4;</i> <i class="material-icons swiperight pointer">&#xE5C8;</i>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 text-right">
               
               <a href="/more">더보기</a>
            </div>
         </div>
      </div>
      
      
<%--       <c:forEach items="${nailList}" var="nail" varStatus="status">
      ${status.count}
      <img class="imgAlbum" src="resources/images/yong/${nail.image}" alt="이미지오류">
      ${nail.name }
      </c:forEach> --%>
      <div class="container container--swipe">
         <div class="row">
            <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6"> -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <c:forEach items="${nailList}" var="nail" varStatus="status">
               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 boxed--radius-sm boxed--shadow bg-color--white marg-b-30">
                  <div class="background-image h-1">
                  
                        <a href="${pageContext.request.contextPath }/index" class="bh">
                           <img src="${pageContext.request.contextPath }/resources/images/yong/${nail.image}" alt="네일 이미지">   
                        </a>
                  
                        <%-- <a href="${pageContext.request.contextPath }/resources/images/yong/${nail.image}" class="bh"></a> --%>
                     <%-- <img class="bh" src="resources/images/yong/${nail.image}"> --%> 
                     <%-- <a href="resources/images/yong/${nail.image}"></a> --%>
                  </div>
               </div>
               </c:forEach>
               <%-- <div class="bg" style="background: url('resources/images/yong/${nail.image}');"></div> --%>
            </div>
         </div>
      </div>
   </section>
   
   <%@ include file="../common/footer.jsp" %>


<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>


<!-- 재윤재윤 -->
<script type="text/javascript" src="<c:url value="/resources/js/jquery.timepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js//jquery-1.8.3.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery/jquery-ui.js?version=1.3"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/external/jquery/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/main.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker.js"/>"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" />  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>  
<script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js"></script>  
<script type="text/javascript" src="<c:url value="/resources/js/timepicker/jquery.timepicker.min.js"/>"></script><!-- 타이머js -->
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/timepicker/jquery.timepicker.css" media=""/><!-- 타이머css -->


<script>
      $(function(){
         $( "#datepicker" ).datepicker({
            /* showOn: "button", */
            /* inline: true, */
            showOtherMonths: true,
            showButtonPanel: true, 
              currentText: '오늘 날짜', 
              closeText: '닫기', 
              dateFormat: "yy-mm-dd",
              changeMonth: true,
              dayNames: ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'],
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            minDate: 0
         });
         $( "#timepicker" ).timepicker({
                     
            
            timeFormat: "h:mm p",
            interval: 60,
            minTime: '10',
            maxTime: '6:00pm',
            defaultTime: '11',
            startTime: '10:00',
            dynamic: false,
            dropdown: true,
            scrollbar: true
         });
      });   
      

</script>

</body>

</html>