<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="resources/https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css">
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

<style>
   .section-custom{
      position: relative;
      z-index: 10;
      padding: 60px 0 10px 0;
   }
</style>



</head>
<body id="body" class="boxed">
   <nav class="nav--gray">
      <div class="container">
         <div class="row row--nav">
            <div class="nav-mod logo left">
               <a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
            </div>
            <div class="nav-mod nav-toggle nav-toggle--side-bar  right">
               <i class="material-icons pointer">&#xE5D2;</i>
            </div>
            <div class="nav-mod side-bar pos-right col-lg-4 col-md-4 col-sm-5 col-xs-12 bg-color--white boxed--shadow">
               <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12 boxed">
                     <i class="material-icons pointer side-bar__close">&#xE14C;</i>
                     <div class="right text--opacity-5">
                        <a href="#" class="fa fa-facebook" aria-hidden="true"></a>
                        <a href="#" class="fa fa-twitter" aria-hidden="true"></a>
                        <a href="#" class="fa fa-google-plus" aria-hidden="true"></a>
                     </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 boxed">
                     <a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 boxed">
                     <ul class="menu menu--toggle-on-click">
                        <div data-include="navigation"></div>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </nav>

   <section class="section-custom">
      <div class="container" style="text-align:center">
         <div class="row marg-t-30">
            <div class="col-md-12 col-sm-5 col-xs-12">
               <c:forEach items="${shopInfo}" var="shop" varStatus="sts">      
                  <h1 class="h3"><strong>${shop.name }</strong></h1>         <!-- shopInfo(네일샵정보)로부터 네일샵 이름 출력 -->
               </c:forEach>
            </div>
         </div>
      </div>
   </section>

   <section>
      <div class="container tabs">
      <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <ul class="tabs__links hr">
                  <li data-tab-link="1" class="active">Design</li>
                  <li data-tab-link="2">Information</li>
               </ul>
            </div>
         </div>
          <div class="tabs__content active" data-tab-content="1">
             <div class="row grid gallery">
                   <c:forEach items="${nailList}" var="nail" varStatus="sts">      <!-- nailList(네일정보목록) 받아옴 -->
                      <div class="col-md-3 col-sm-3 col-xs-6 grid-item grid-sizer">
                        <a href="${pageContext.request.contextPath }/nailDetail?id=1" class="">     <!-- 네일 선택 시 상품 상세 페이지로 이동 -->
                        <img src="resources/images/young/${nail.image }" width="300" height="300" alt="Nail">      <!-- 네일 이미지 출력 -->
                        <p class="p-xs">${nail.name }  </p>      <!-- 네일이름 출력 -->
                        </a>                     
                     <p class="p-xs">${nail.price }  </p>         <!-- 네일 가격 출력 -->
                  </div>
               </c:forEach>
             </div>
         </div>

         <div class="tabs__content" data-tab-content="2">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <c:forEach items="${shopInfo}" var="shop" varStatus="sts">         <!-- shopInfo(네일샵정보)로부터 받아옴 -->
               <h3>${shop.name }<!-- Nail Shop Name --> <span class="center"></span></h3>
                  <h6 class="marg-t-30">영업시간 : ${shop.time } <!-- 네일샵 영업시간  --></h6>
                  <h6 class="marg-t-30">연락처  : ${shop.phone } <!-- 네일샵 연락처 --> </h6>
                  <h6 class="marg-t-30">이메일  : ${shop.email } <!-- 네일샵 이메일 --> </h6>
                  <h6 class="marg-t-30">주소 :   ${shop.address } <!-- 네일샵 주소(위치) --> </h6>
               </c:forEach>
            </div>

            <div class="container-fluid padding-null">
               <div class="row row--fluid">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <!-- 네일샵 위치 구글맵 -->
                     <div id="map" style="width:100%;height:400px;">
                        <!-- 네일샵 주소 -->
                        <c:forEach items="${shopInfo}" var="shop" varStatus="sts">
                           <iframe src="${shop.map_address }"
                              frameborder="0" 
                              style="border:0" 
                              height="450"
                              class="width-full" 
                              allowfullscreen>
                           </iframe>
                        </c:forEach>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

   <footer class="bg-color--white-2">
      <div class="container">
         <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
               <a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
               <div class="row offset-t-15">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                     <i>Made with love by Wave Themes</i><br>
                  </div>
               </div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
               <h6>Our company</h6>
               <ul class="ul--list ul-xs">
                  <li><a href="#">About</a></li>
                  <li><a href="#">Work</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Articles</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
               <h6>Categories</h6>
               <ul class="ul--list ul-xs">
                  <li><a href="#">Mans</a></li>
                  <li><a href="#">Womans</a></li>
                  <li><a href="#">Summer</a></li>
                  <li><a href="#">Winter</a></li>
               </ul>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
               <h6>Tags</h6>
               <ul class="ul--list ul-xs">
                  <li><a href="#">Adventure</a></li>
                  <li><a href="#">Business</a></li>
                  <li><a href="#">Art</a></li>
                  <li><a href="#">Trending</a></li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6 col-md-offset-6 col-sm-12 col-xs-12">
               <a href="#" class="fa fa-facebook" aria-hidden="true"></a>
               <a href="#" class="fa fa-twitter" aria-hidden="true"></a>
               <a href="#" class="fa fa-google-plus" aria-hidden="true"></a>
            </div>
         </div>
      </div>
   </footer>



<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>
<!-- <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=bd270a3511e4864054373879fd129474"></script> -->
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4exBmUUuGIPw952oNnqEvXR1MT7X-KI0&callback=initMap"></script>



                        
</body>

</html>