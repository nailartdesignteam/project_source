<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="resources/https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css">
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

</head>
<body id="body" class="boxed">

	
	<%@ include file="../common/header.jsp" %>


	<nav class="nav--gray">
		<div class="container">
			<div class="row row--nav">
				<div class="nav-mod logo left">
					<a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
				</div>

				<div class="nav-mod side-bar pos-right col-lg-4 col-md-4 col-sm-5 col-xs-12 bg-color--white boxed--shadow">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 boxed">
							<i class="material-icons pointer side-bar__close">&#xE14C;</i>
							<div class="right text--opacity-5">
								<a href="#" class="fa fa-facebook" aria-hidden="true"></a>
								<a href="#" class="fa fa-twitter" aria-hidden="true"></a>
								<a href="#" class="fa fa-google-plus" aria-hidden="true"></a>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 boxed">
							<a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 boxed">
							<ul class="menu menu--toggle-on-click">
								<div data-include="navigation"></div>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>

	<section>
		<div class="container tabs">
			<div class="">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h3>Nail Shop</h3>
					<hr size="5">
				</div>
			</div>
			
			
			<div class="tabs__content active" data-tab-content="1">
				
				<c:forEach items="${shopList }" var="shop" varStatus="sts">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<h6>${shop.name }<!-- 연재의 네일샵 --><span class="right">
						<a href="${pageContext.request.contextPath }/shopMain?id=${shop.id}" class="btn btn-sm btn--line">보러가기</a><br></span></h6>  <!-- 버튼 클릭 시 네일샵 메인페이지로 이동 -->
						<p class="p-xs">${shop.content }<!-- #20대 #심플베이직 --></p>					
					</div>					
				</c:forEach>
			</div>
		</div>
	</section>

	<footer class="bg-color--white-2">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<a href="index.html"><img alt="" src="images/logo--black.png" height="22"></a>
					<div class="row offset-t-15">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<i>Made with love by Wave Themes</i><br>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6>Our company</h6>
					<ul class="ul--list ul-xs">
						<li><a href="#">About</a></li>
						<li><a href="#">Work</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Articles</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6>Categories</h6>
					<ul class="ul--list ul-xs">
						<li><a href="#">Mans</a></li>
						<li><a href="#">Womans</a></li>
						<li><a href="#">Summer</a></li>
						<li><a href="#">Winter</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6>Tags</h6>
					<ul class="ul--list ul-xs">
						<li><a href="#">Adventure</a></li>
						<li><a href="#">Business</a></li>
						<li><a href="#">Art</a></li>
						<li><a href="#">Trending</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-6 col-sm-12 col-xs-12">
					<a href="#" class="fa fa-facebook" aria-hidden="true"></a>
					<a href="#" class="fa fa-twitter" aria-hidden="true"></a>
					<a href="#" class="fa fa-google-plus" aria-hidden="true"></a>
				</div>
			</div>
		</div>
	</footer>



<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>
