<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

<style>

	.font-black{
		color: #000000;
	}

</style>


</head>  

<body id="body">	
		<nav class="nav--light nav--gray nav--absolute">
		<div class="container">
			<div class="row row--nav row--nav--baseline">
				<div class="nav-mod logo left">
					<a href="index.html"><img alt="" src="resources/images/logo--white.png" height="22"></a>
				</div>
				<div class="nav-mod nav-toggle right visible-xs visible-sm">
					<i class="material-icons pointer">&#xE5D2;</i>
				</div>
				<div class="nav-mod nav hidden-xs hidden-sm left">
					<ul class="menu text--uppercase">
						
						<li class="hr visible-xs visible-sm">
							<form><input type="search" placeholder="Type and hit enter to search"></form>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/reservation1"><span class="font-black">네일샵 예매</span></a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="../main/test.jsp">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><span class="font-black">디자인 공유</span></a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="../main/test.jsp">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#"><span class="font-black">디자인 색 추천</span></a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="../main/test.jsp">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/shopList"><span class="font-black">네일샵 정보</span></a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="../main/test.jsp">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="${pageContext.request.contextPath }/myPage"><span class="font-black">마이페이지</span></a>
							<ul class="menu__dropdown boxed ul--split">									
								<li>
							    	<a href="../main/test.jsp">Accordion</a>
								</li>
								<li>
							    	<a href="elements-animate.html">Animate</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="nav-mod-group right left-xs left-sm hidden-xs">
					<div class="nav-mod">
						<a href="https://themeforest.net/item/rush-multipurpose-html-with-page-builder/19905370" class="btn btn-sm btn--default">Buy now</a>
					</div>
					<div class="nav-mod">
						<a href="main/test.jsp" target="_blank" class="btn btn-sm btn--line">Try Builder</a>
					</div>
				</div>
			</div>
		</div>
	</nav>
	



<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>