<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<!DOCTYPE html>
<html lang="utf-8">
<head>
<meta charset="utf-8">
<title>Rush</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="robots" content="all"/>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta property="og:image" content="http://placehold.it/100x100" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" type="text/css">
<link rel="stylesheet" href="resources/css/ico/pixedenico/Pe-icon-7-stroke.css" type="text/css"  />
<link rel="stylesheet" href="resources/css/ico/font-awesome/css/font-awesome.min.css" type="text/css"  />
<link rel="stylesheet" href="resources/bootstrap/css/bootstrap.css" type="text/css" media="screen" />
<link rel="stylesheet" href="resources/js/arcticmodal/jquery.arcticmodal-0.3.css"> 
<link rel="stylesheet" href="resources/css/main/transitions.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/animate.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/style.css" type="text/css" />
<link rel="stylesheet" href="resources/css/typography/OpenSans.css" type="text/css" />
<link rel="stylesheet" href="resources/css/main/theme-main.css" type="text/css" />
<link rel="stylesheet" href="resources/css/user.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.css" type="text/css" />
<link rel="stylesheet" href="resources/js/picker/themes/classic.date.css" type="text/css" />
<link rel="stylesheet" href="resources/js/prismjs/prism.css"  data-noprefix />

</head>  

<body id="body">	


	<footer class="bg-color--white">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<a href="index.html"><img alt="" src="resources/images/logo--black.png" height="22"></a>
					<div class="row offset-t-15">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<i>Made with love by Wave Themes</i><br>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6 class="ul--toggle__title">Our company</h6>
					<ul class="ul--list ul-xs ul--toggle">
						<li><a href="#">About</a></li>
						<li><a href="#">Work</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Articles</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6 class="ul--toggle__title">Categories</h6>
					<ul class="ul--list ul-xs ul--toggle">
						<li><a href="#">Mans</a></li>
						<li><a href="#">Womans</a></li>
						<li><a href="#">Summer</a></li>
						<li><a href="#">Winter</a></li>
					</ul>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-12">
					<h6 class="ul--toggle__title">Tags</h6>
					<ul class="ul--list ul-xs ul--toggle">
						<li><a href="#">Adventure</a></li>
						<li><a href="#">Business</a></li>
						<li><a href="#">Art</a></li>
						<li><a href="#">Trending</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	


<script type="text/javascript" src="resources/js/jquery-git.min.js"></script>
<script type="text/javascript" src="resources/js/instafeed.js"></script>
<script type="text/javascript" src="resources/js/countdown-master/dest/jquery.countdown.min.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.js"></script>
<script type="text/javascript" src="resources/js/picker/picker.date.js"></script>
<script type="text/javascript" src="resources/js/prismjs/prism.js"></script>
<script type="text/javascript" src="resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/animate.js"></script>
<script type="text/javascript" src="resources/js/waypoints.js"></script>
<script type="text/javascript" src="resources/js/FlexSlider-release-2-2-0/jquery.flexslider.js"></script>
<script type="text/javascript" src="resources/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="resources/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="resources/js/typed.js"></script>
<script type="text/javascript" src="resources/js/main.js"></script>
<script type="text/javascript" src="resources/js/user.js"></script>
<script type="text/javascript" src="resources/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<script type="text/javascript" src="resources/js/placeholder/placeholder.js"></script>
<script type="text/javascript" src="resources/js/jquery.mobile-git.min.js"></script>

</body>

</html>

