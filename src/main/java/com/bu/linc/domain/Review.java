package com.bu.linc.domain;

import java.sql.Date;

//예약
public class Review {

	private int id; 			//리뷰 아이디(인덱스)
	private String userId; 		//회원 아이디
	private Date date; 			//리뷰 작성날짜
	private String content;	 	//리뷰 내용
	private String image; 		//리뷰 이미지
      
	private NailShop nailShop;	//네일샵
	//(네일샵 - 네일샵명)(네일 - 네일명)(네일 - 네일이미지)

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public NailShop getNailShop() {
		return nailShop;
	}

	public void setNailShop(NailShop nailShop) {
		this.nailShop = nailShop;
	}
   
}
