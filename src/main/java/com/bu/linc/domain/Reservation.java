package com.bu.linc.domain;

import java.sql.Date;

//�삁�빟
public class Reservation {

	private int id; 			//�삁�빟 �븘�씠�뵒(�씤�뜳�뒪)
	private String userId; 		//�쉶�썝 �븘�씠�뵒
	private Date date; 			//�삁�빟 �궇吏�
	private String time; 		//�삁�빟 �떆媛�
	private String designer;
	private String nailshopId;
	private int nailId; 		//�삁�빟 吏곸썝
	

	public int getNailId() {
		return nailId;
	}

	public void setNailId(int nailId) {
		this.nailId = nailId;
	}

	public String getNailshopId() {
		return nailshopId;
	}

	public void setNailshopId(String nailshopId) {
		this.nailshopId = nailshopId;
	}

	private NailShop nailShop;	//�꽕�씪�꺏
	//(�꽕�씪�꺏 - �꽕�씪�꺏紐�)(�꽕�씪 - �꽕�씪紐�)(�꽕�씪 - �꽕�씪媛�寃�)(�꽕�씪 - �꽕�씪�씠誘몄�)
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDesigner() {
		return designer;
	}

	public void setDesigner(String designer) {
		this.designer = designer;
	}

	public NailShop getNailShop() {
		return nailShop;
	}

	public void setNailShop(NailShop nailShop) {
		this.nailShop = nailShop;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}