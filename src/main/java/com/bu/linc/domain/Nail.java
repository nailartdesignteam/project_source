package com.bu.linc.domain;

//네일 상품
public class Nail{
	
	private int id;					//네일 아이디(인덱스)
	private String image;			//이미지명
	private String name;			//네일 이름
	private String price;			//네일 상품 가격
	private String content;			//네일 내용
	private String nailShopName;	//네일 샵 이름
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getNailShopName() {
		return nailShopName;
	}
	public void setNailShopName(String nailShopName) {
		this.nailShopName = nailShopName;
	}
	


}
