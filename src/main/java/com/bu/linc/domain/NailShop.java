package com.bu.linc.domain;

import java.util.List;

//네일샵
public class NailShop{
   
   private String id;            	//네일샵 아이디
   private String password;      	//네일샵 패스워드 
   private String name;         	//네일샵 이름
   private String content;         	//네일샵 소개
   private String phone;         	//네일샵 전화번호
   private String email;         	//네일샵 이메일
   private String address;         	//네일샵 주소
   private String time;         	//네일샵 영업시간
   private String possible_date;   	//네일샵 예약가능날짜
   private String possible_time;   	//네일샵 예약가능시간
   private String designer;      	//네일샵 디자이너
   private String map_address;      //네일샵 위치(지도)
   
   
   private List<Nail> nails;      	//네일의 정보


   public String getId() {
      return id;
   }


   public void setId(String id) {
      this.id = id;
   }


   public String getPassword() {
      return password;
   }


   public void setPassword(String password) {
      this.password = password;
   }


   public String getName() {
      return name;
   }


   public void setName(String name) {
      this.name = name;
   }


   public String getContent() {
      return content;
   }


   public void setContent(String content) {
      this.content = content;
   }


   public String getPhone() {
      return phone;
   }


   public void setPhone(String phone) {
      this.phone = phone;
   }


   public String getEmail() {
      return email;
   }


   public void setEmail(String email) {
      this.email = email;
   }


   public String getAddress() {
      return address;
   }


   public void setAddress(String address) {
      this.address = address;
   }


   public String getTime() {
      return time;
   }


   public void setTime(String time) {
      this.time = time;
   }


   public String getPossible_date() {
      return possible_date;
   }


   public void setPossible_date(String possible_date) {
      this.possible_date = possible_date;
   }


   public String getPossible_time() {
      return possible_time;
   }


   public void setPossible_time(String possible_time) {
      this.possible_time = possible_time;
   }


   public String getDesigner() {
      return designer;
   }


   public void setDesigner(String designer) {
      this.designer = designer;
   }


   public String getMap_address() {
      return map_address;
   }


   public void setMap_address(String map_address) {
      this.map_address = map_address;
   }


   public List<Nail> getNails() {
      return nails;
   }


   public void setNails(List<Nail> nails) {
      this.nails = nails;
   }

   
}