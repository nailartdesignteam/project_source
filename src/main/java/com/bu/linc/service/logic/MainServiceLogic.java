package com.bu.linc.service.logic;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.MainDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.service.MainService;

@Service
public class MainServiceLogic implements MainService{

   private Logger logger = Logger.getLogger(getClass());
   
   @Autowired
   private MainDao mainDao;
   
   @Override
   public List<Nail> readAll() {
      logger.info(">>>>>>>>>>>> MainServiceLogic readAll Complete");
      return mainDao.readAll();
   }

@Override
public List<NailShop> mainSearch(String shop, String designer, String date, String time) {
   
   return mainDao.findDate(shop, designer, date, time);
}

}