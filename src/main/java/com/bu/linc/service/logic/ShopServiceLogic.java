package com.bu.linc.service.logic;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.ShopDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.service.ShopService;

@Service
public class ShopServiceLogic implements ShopService{

	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private ShopDao dao;

	@Override
	public List<NailShop> findAllShop() {				// 네일샵 전체 목록
		// TODO Auto-generated method stub
		return dao.readAllShop();
	}

	@Override
	public List<Nail> readById(String id) {				// 각 네일샵마다 해당하는 네일 목록
		// TODO Auto-generated method stub
		return dao.findById(id);
	}

	@Override
	public List<NailShop> readShopInfo(String id) {		// 네일샵 정보
		// TODO Auto-generated method stub
		return dao.shopInfo(id);
	}
	

	@Override
	public NailShop findShop(String id) {
		// TODO Auto-generated method stub

		logger.info(">>>>> id " + id);
		return dao.read(id);
	}

	@Override
	public Nail findNail(String id) {
		// TODO Auto-generated method stub
		return dao.readNail(id);
	}

	@Override
	public List<Nail> findAllNail() {
		// TODO Auto-generated method stub
		return dao.readAllNail();
	}
	

}
