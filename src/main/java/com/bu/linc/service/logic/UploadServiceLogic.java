package com.bu.linc.service.logic;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.UploadDao;
import com.bu.linc.service.UploadService;

@Service
public class UploadServiceLogic implements UploadService{
	
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private UploadDao uploadDao;

	@Override
	public boolean insert(int id, String image, String extension) {
		logger.info(">>>>>>>>>>>> UploadServiceLogic INSERT Complete");
		System.out.println("111111111111111111111111");
		System.out.println("id : " +id + "image : " + image + "extension : " + extension);
		return uploadDao.insert(id, image, extension);
	}
	
}