package com.bu.linc.service.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.ReviewDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.Review;
import com.bu.linc.service.ReviewService;

@Service
public class ReviewServiceLogic implements ReviewService {

	@Autowired
	private ReviewDao reviewDao;
	
	@Override
	public List<Review> findAllReviews() {
		
		return reviewDao.retrieveAll();
	}
	

	@Override
	public void registerReview(Review review) {
		reviewDao.getDate();
		//reviewDao.getNext();
		//reviewDao.write(review.getUserId(), review.getContent(), review.getImage());
		reviewDao.write(review);
		
	}

	@Override
	public Review read(String userId) {
		
		return reviewDao.read(userId);
	}

	@Override
	public String findUserId() {
		
		return reviewDao.getUserId();
	}


	@Override
	public List<NailShop> findName(String shopId,String userId) {
		// TODO Auto-generated method stub
		return reviewDao.retrieveId(shopId,userId);
	}


	@Override
	public Reservation findAllReservation(String userId) {
		
		return reviewDao.find(userId);
	}


	@Override
	public String findShopId() {
		
		return reviewDao.getShopId();
	}


	@Override
	public Nail findNailName(String userId) {
		// TODO Auto-generated method stub
		return reviewDao.findNailName(userId);
	}


	
}
