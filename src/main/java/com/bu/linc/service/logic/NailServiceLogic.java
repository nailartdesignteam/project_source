package com.bu.linc.service.logic;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.NailDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.Review;
import com.bu.linc.service.NailService;

@Service
public class NailServiceLogic implements NailService {
	
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private NailDao dao;

	@Override
	public Nail findNail(int id) {
		// TODO Auto-generated method stub
		logger.info(">>>>>>>> serviceLogic findNail id : " + id);
		return dao.find(id);
	}

	@Override
	public List<Review> findAllReviews(int id) {
		logger.info(">>>>>>>>>>>> Service 2222222222222");
		logger.info(">>>>>>>>>>>> nailId : " + id);
		// TODO Auto-generated method stub
		return dao.findAll(id);
	}

}
