package com.bu.linc.service.logic;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.WishListDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.WishList;
import com.bu.linc.service.WishListService;

@Service
public class WishListServiceLogic implements WishListService{
	
	@Autowired
	private WishListDao dao;

	@Override
	public boolean addWishList(String userId, int nailId) {
		return dao.add(userId, nailId);
	}

	@Override
	public void removeWishList(int id) {
		dao.delete(id);	
	}

	@Override
	public List<Nail> findAllWishLists(String userId, String nailId) {
		return dao.findAll(userId,userId);
	}

	@Override
	public List<Nail> findNailInfo(int nailId) {
		return dao.findNail(nailId);
	}

}
