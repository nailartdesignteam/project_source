package com.bu.linc.service.shop;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;


public interface ShopService {

	List<NailShop> findAllShop();				// 네일샵 전체 목록
	List<Nail> readById(String id);				// 각 네일샵마다 해당하는 네일 목록
	List<NailShop> readShopInfo(String id);		// 네일샵 정보
	NailShop findShop(String id);				// 네일샵 
	Nail findNail(String id);					// 네일
	List<Nail> findAllNail();					// 네일 전체 
	
}