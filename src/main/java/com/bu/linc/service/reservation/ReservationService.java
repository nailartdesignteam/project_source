package com.bu.linc.service.reservation;

import java.sql.Date;
import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;

public interface ReservationService {

   void registReservation(String shopId, String userId, int nailId);
   Reservation findReservation(String userId);
   void removeReservation(String id, String reservationId);
   List<NailShop> findNailshop(String date, String time);
   List<Reservation> readAll();
   List<NailShop> findByShopId(String id);
   List<Nail> findNail(String id);
   String findUserId();
   List<Nail> findByNailId(int nailId);
}