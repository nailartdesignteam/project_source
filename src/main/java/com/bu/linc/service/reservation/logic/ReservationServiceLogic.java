package com.bu.linc.service.reservation.logic;
import java.sql.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bu.linc.dao.reservation.logic.ReservationDaoLogic;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.service.reservation.ReservationService;

@Service
public class ReservationServiceLogic implements ReservationService{

   private Logger logger = Logger.getLogger(getClass());
   
   @Autowired
   private ReservationDaoLogic reservationDao;

   @Override
   public void registReservation(String shopId, String userId, int nailId) {
      // TODO Auto-generated method stub
      reservationDao.insert(shopId, userId, nailId);
   }
   
   @Override
   public Reservation findReservation(String userId) {
      // TODO Auto-generated method stub
      return reservationDao.find(userId);
   }
   
   @Override
   public void removeReservation(String id, String reservationId) {
      // TODO Auto-generated method stub
      reservationDao.delete(id, reservationId);
   }

   @Override
   public List<NailShop> findNailshop(String date, String time) {
      // TODO Auto-generated method stub
      logger.info(">>>>>>>> Service Logic date : " + date);
      logger.info(">>>>>>>> Service Logic time : " + time);
      return reservationDao.findDate(date, time);
   }
   
   
   @Override
   public List<Reservation> readAll() {
      // TODO Auto-generated method stub
      return reservationDao.readAll();
   }

   @Override
   public List<NailShop> findByShopId(String id) {
      
      return reservationDao.findById(id);
   }

   @Override
   public List<Nail> findNail(String id) {
      // TODO Auto-generated method stub
      return reservationDao.findByShopId(id);
   }

   @Override
   public String findUserId() {
      // TODO Auto-generated method stub
      return reservationDao.findUserId();
   }

   @Override
   public List<Nail> findByNailId(int nailId) {
      
      return reservationDao.findByNailId(nailId);
   }
   


}