package com.bu.linc.service.review;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.Review;

public interface ReviewService {

	Reservation findAllReservation(String userId);
	List<Review> findAllReviews();
	List<NailShop> findName(String shopId,String userId);
	//NailShop readName();
    //public Review findMyReview(String userId);
	void registerReview(Review review);
	Review read(String userId);
	String findUserId();
	String findShopId();
	Nail findNailName(String userId);
	


}
