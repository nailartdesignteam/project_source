package com.bu.linc.service.main;

import org.springframework.stereotype.Service;

public interface UploadService {

	public boolean insert(int id, String image, String extension);
	
}
