package com.bu.linc.service.main;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;

public interface MainService {
   
   public List<Nail> readAll();      //이미지 출력
   List<NailShop> mainSearch(String shop, String designer, String date, String time);
 
}