package com.bu.linc.service;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.Review;

public interface NailService {

	Nail findNail(int id);
	List<Review> findAllReviews(int id);
}
