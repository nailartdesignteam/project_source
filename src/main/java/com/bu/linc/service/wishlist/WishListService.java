package com.bu.linc.service.wishlist;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.WishList;

public interface WishListService {
	
	boolean addWishList(String userId, int nailId);
	void removeWishList(int id);
	List<Nail> findAllWishLists(String userId, String nailId);
	List<Nail> findNailInfo(int nailid);
}
