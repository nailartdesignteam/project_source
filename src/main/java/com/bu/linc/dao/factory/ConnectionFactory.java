package com.bu.linc.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//import org.apache.log4j.Logger;

public class ConnectionFactory {

	//private Logger logger = Logger.getLogger(getClass());
	
	private static ConnectionFactory instance;

	private static final String DRIVER_NAME = "oracle.jdbc.driver.OracleDriver"; 	//oracle ����̹� �̸�
	private static final String URL = "jdbc:oracle:thin:@nailartdb.cr4ws6zdhijm.ap-northeast-2.rds.amazonaws.com:1521:naildb";		//DB���� URL
	private static final String USER_NAME = "nailartmaster";						//������̸�
	private static final String PASSWORD = "lablab1123";							//��й�ȣ
	
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER_NAME);
			//logger.info(">>>>>>> 1. JDBC ConnectionFactory Complete");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public static ConnectionFactory getInstance() {
		if (instance == null) {
			instance = new ConnectionFactory();
			System.out.println(">>>>>>> 2. JDBC ConnectionFactory getInstance Complete");	
		}
		return instance;
	}

	public Connection createConnection() throws SQLException {
		//logger.info(">>>>>>> 3. JDBC createConnection Complete");
		return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
	}
}