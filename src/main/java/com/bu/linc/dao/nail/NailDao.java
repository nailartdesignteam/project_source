package com.bu.linc.dao.nail;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.Review;

public interface NailDao {
	
	Nail find(int id);
	List<Review> findAll(int id);
}
