package com.bu.linc.dao.main;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;

public interface MainDao {
   
   List<Nail> readAll();      //이미지 출력
   List<NailShop> findDate(String shop,String designer, String date, String time);
}