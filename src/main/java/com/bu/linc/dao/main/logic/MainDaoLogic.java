package com.bu.linc.dao.main.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.main.MainDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;

@Repository
public class MainDaoLogic implements MainDao {

   private Logger logger = Logger.getLogger(getClass());
   
   private ConnectionFactory connectionFactory;
   
   public MainDaoLogic() {
      connectionFactory = ConnectionFactory.getInstance();
   }
   
   //이미지 출력
   @Override
   public List<Nail> readAll() {
      
      Connection connection = null;
      Statement stmt = null;
      ResultSet rs = null;
      List<Nail> nails = new ArrayList<Nail>();
       
      try {
         logger.info(">>>>>>>>>>>> TRY Complete");
         connection = connectionFactory.createConnection();
         stmt = connection.createStatement();

         String SQL = "SELECT IMAGE FROM NAIL";         
         rs = stmt.executeQuery(SQL);
         logger.info(">>>>>>>>>>>> SQL QUERY executeQuery Complete");
         
         while(rs.next()) {
            Nail nail = new Nail();
            nail.setImage(rs.getString(1));
            nails.add(nail);
         }
      } catch(SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, stmt, connection);
         logger.info(">>>>>>>>>>>> JDBC RESOURCE close Complete");
      }
      
      return nails;
   }

@Override
public List<NailShop> findDate(String shop, String designer, String date, String time) {
   String sql = "SELECT ID, PASSWORD, NAME, PHONE, EMAIL, ADDRESS, TIME, CONTENT, DESIGNER FROM NAILSHOP WHERE NAME = ? AND DESIGNER = ? AND POSSIBLE_DATE = TO_DATE(?,'yyyy-mm-dd') AND POSSIBLE_TIME LIKE ?";
   logger.info(">>>>>>>>>>>>>> shop : " + shop);
   logger.info(">>>>>>>>>>>>>> designer : " + designer);
   logger.info(">>>>>>>>>>>>>> date : " + date);
   logger.info(">>>>>>>>>>>>>> time : " + time);
   Connection conn = null;
   PreparedStatement psmt = null;
   ResultSet rs = null;
   List<NailShop> nailshops = new ArrayList<NailShop>();

   /*
    * java.sql.Date realDate = java.sql.Date.valueOf(date");
    * 
    * logger.info(">>>>>>>>>>>>>> realDate : " + realDate.toString());
    */
   try {
      conn = connectionFactory.createConnection();
      psmt = conn.prepareStatement(sql.toString());
//      psmt.setDate(parameterIndex, x);
//      (1,"%"+date+"%");
      psmt.setString(1, shop);
      psmt.setString(2, designer);
      psmt.setString(3, date);
      psmt.setString(4, "%"+time+"%");
      rs = psmt.executeQuery();

      while (rs.next()) {
         NailShop nailshop = new NailShop();
         nailshop.setId(rs.getString(1));
         nailshop.setPassword(rs.getString(2));
         nailshop.setName(rs.getString(3));
         nailshop.setPhone(rs.getString(4));
         nailshop.setEmail(rs.getString(5));
         nailshop.setAddress(rs.getString(6));
         nailshop.setTime(rs.getString(7));
         nailshop.setContent(rs.getString(8));
         nailshop.setDesigner(rs.getString(9));
         nailshops.add(nailshop);
      }
   } catch (SQLException e) {
      throw new RuntimeException(e);
   } finally {
      JdbcUtils.close(rs, psmt, conn);
   }

   return nailshops;
}
   
}