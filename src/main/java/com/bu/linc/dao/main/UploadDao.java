package com.bu.linc.dao.main;

import java.sql.SQLException;
import java.util.List;

import com.bu.linc.domain.FileTest;

public interface UploadDao {

	boolean insert(int id, String image, String extension);
}
