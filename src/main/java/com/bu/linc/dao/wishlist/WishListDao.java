<<<<<<< HEAD
package com.bu.linc.dao.wishlist;

import java.util.List;

import com.bu.linc.domain.WishList;

public interface WishListDao {
	
	String add(WishList wishList);
	void delete(int id);
	List<WishList> findAll(String userId);
    
}
=======
package com.bu.linc.dao.wishlist;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.WishList;

public interface WishListDao {
	
	boolean add(String userId, int nailId);
	void delete(int id);
	List<Nail> findAll(String userId,String nailId);
    List<Nail> findNail(int nailId);
}
>>>>>>> branch 'master' of https://gitlab.com/nailartdesignteam/project_source.git
