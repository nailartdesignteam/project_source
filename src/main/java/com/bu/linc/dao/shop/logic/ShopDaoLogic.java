<<<<<<< HEAD
package com.bu.linc.dao.shop.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.shop.ShopDao;

@Repository
public class ShopDaoLogic implements ShopDao{

	private ConnectionFactory connectionFactory;
	
	public ShopDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

/*	@Override
	public NailShop read(String shopId) {
		
		String sql = "SELECT SHOP_ID, SHOP_NAME, SHOP_PHONE, SHOP_EMAIL, SHOP_ADDRESS, SHOP_TIME FROM NAILSHOP WHERE SHOP_ID = ?" ;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		NailShop shop = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, shopId);
			rs = psmt.executeQuery();
			if (rs.next()) {
				shop = new NailShop();
				shop.setShopId(rs.getString(1));
				shop.setShopName(rs.getString(2));
				shop.setFeature(rs.getString(3));
				shop.setPhone(rs.getString(4));
				shop.setEmail(rs.getString(5));
				shop.setAddress(rs.getString(6));
				shop.setTime(rs.getString(7));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return shop;
	}

	@Override
	public List<NailShop> readAllShop() {
		
		String sql = "SELECT SHOP_ID, SHOP_NAME, SHOP_PHONE, SHOP_EMAIL, SHOP_ADDRESS, SHOP_TIME FROM NAILSHOP" ;

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<NailShop> shops = new ArrayList<NailShop>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				NailShop shop = new NailShop();
				shop.setShopId(rs.getString(1));
				shop.setShopName(rs.getString(2));
				shop.setFeature(rs.getString(3));
				shop.setPhone(rs.getString(4));
				shop.setEmail(rs.getString(5));
				shop.setAddress(rs.getString(6));
				shop.setTime(rs.getString(6));
				shops.add(shop);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}

		return shops;
	}
	*/
}
=======
package com.bu.linc.dao.shop.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.shop.ShopDao;
import com.bu.linc.domain.NailShop;

@Repository
public class ShopDaoLogic implements ShopDao{

	private ConnectionFactory connectionFactory;
	
	public ShopDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public NailShop read(String id) {
		
		String sql = "SELECT ID, NAME, CONTENT, PHONE, EMAIL, ADDRESS, TIME FROM NAILSHOP WHERE ID = ?" ;

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		NailShop shop = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			if (rs.next()) {
				shop = new NailShop();
				shop.setId(rs.getString(1));
				shop.setName(rs.getString(2));
				shop.setContent(rs.getString(3));
				shop.setPhone(rs.getString(4));
				shop.setEmail(rs.getString(5));
				shop.setAddress(rs.getString(6));
				shop.setTime(rs.getString(7));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return shop;
	}

	@Override
	public List<NailShop> readAllShop() {
		
		String sql = "SELECT ID, NAME, CONTENT, PHONE, EMAIL, ADDRESS, TIME FROM NAILSHOP" ;

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<NailShop> shops = new ArrayList<NailShop>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				NailShop shop = new NailShop();
				shop.setId(rs.getString(1));
				shop.setName(rs.getString(2));
				shop.setContent(rs.getString(3));
				shop.setPhone(rs.getString(4));
				shop.setEmail(rs.getString(5));
				shop.setAddress(rs.getString(6));
				shop.setTime(rs.getString(7));
				shops.add(shop);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}

		return shops;
	}
	
}
>>>>>>> branch 'master' of https://gitlab.com/nailartdesignteam/project_source.git
