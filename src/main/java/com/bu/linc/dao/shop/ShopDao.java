package com.bu.linc.dao.shop;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;

public interface ShopDao {
   
   List<NailShop> readAllShop();         // 네일샵 전체 목록
   List<Nail> findById(String id);         // 각 네일샵마다 해당하는 네일 목록
   List<NailShop> shopInfo(String id);      // 네일샵 정보
   NailShop read(String id);
   Nail readNail(String id);
   List<Nail> readAllNail();

}