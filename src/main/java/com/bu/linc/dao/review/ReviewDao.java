package com.bu.linc.dao.review;

import java.util.List;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.Review;

public interface ReviewDao {
	
	Reservation find(String userId);
	List<Review> retrieveAll();
	List<NailShop> retrieveId(String shopId, String userId);
	//NailShop retrieveName();
	String getDate();
	//int getNext();
	//boolean write(String userId, String content, String image );
	boolean write(Review review);
	Review read(String userId);
	String getUserId();
	String getShopId();
	Nail findNailName(String userId);
}
