package com.bu.linc.dao.review.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.review.ReviewDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.Review;
import com.bu.linc.domain.User;


@Repository
public class ReviewDaoLogic implements ReviewDao{

	private Logger logger = Logger.getLogger(getClass());
	
	private ConnectionFactory connectionFactory;
	
	public ReviewDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	@Override
	public List<Review> retrieveAll() {
		
		String sql = "SELECT ID, REVIEW_DATE, CONTENT, IMAGE, SHOP_ID, USER_ID, NAIL_ID FROM REVIEW";
		//String sql = "select nail.name, nailshop.name, nail.image, review.content, review.image, review.review_date, review.user_id from nail, nailshop, reviewwhere review.nail_id = nail.id and review.shop_id = nailshop.id" ;
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Review> reviews = new ArrayList<Review>();
		NailShop shop = new NailShop();
		Nail nails = new Nail();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				/*
				 * Review review = new Review(); nails.setName(rs.getString(1));
				 * shop.setName(rs.getString(2)); nails.setImage(rs.getString(3));
				 * review.setContent(rs.getString(4)); review.setImage(rs.getString(5));
				 * review.setDate(rs.getDate(6)); review.setUserId(rs.getString(7));
				 * reviews.add(review);
				 */
				Review review = new Review();
				review.setId(rs.getInt(1));
				review.setDate(rs.getDate(2));
				review.setContent(rs.getString(3));
				review.setImage(rs.getString(4));
				shop.setId(rs.getString(5));
				review.setUserId(rs.getString(6));
				nails.setId(rs.getInt(7));
				reviews.add(review);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}

		return reviews;
	}

	@Override
	public List<NailShop> retrieveId(String shopId, String userId) {
		
		String sql = "select nailshop.name from nailshop, review, user_tb where ? = review.user_id and ? = review.shop_id";
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<NailShop> shops = new ArrayList<NailShop>();
		

		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql.toString());
			pstmt.setString(1, userId);
			pstmt.setString(2, shopId);
			rs = pstmt.executeQuery();
		
			while (rs.next()) {
			    NailShop shop = new NailShop();
				shop.setName(rs.getString(1));
				shops.add(shop);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, pstmt, conn);
		}

		return shops;
	}
	

	@Override
	public String getDate() {
		
		String sql = "SELECT SYSDATE FROM DUAR";
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				return rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, pstmt, conn);
		}
		return ""; //�뜲�씠�꽣踰좎씠�뒪 �삤瑜�
		
	}
	
	
	//由щ럭 踰덊샇 
	/*
	 * @Override public int getNext() {
	 * 
	 * String sql = "SELECT ID FROM REVIEW ORDER BY ID DESC";
	 * 
	 * Connection conn = null; PreparedStatement pstmt = null; ResultSet rs = null;
	 * 
	 * try { conn = connectionFactory.createConnection(); pstmt =
	 * conn.prepareStatement(sql); rs = pstmt.executeQuery(); if(rs.next()) { return
	 * rs.getInt(1)+1; } return 1; //泥ル쾲吏� 寃뚯떆臾쇱씤 寃쎌슦 } catch (SQLException e) {
	 * e.printStackTrace(); //throw new RuntimeException(e); } finally {
	 * JdbcUtils.close(rs, pstmt, conn); } return -1; //�뜲�씠�꽣踰좎씠�뒪 �삤瑜� }
	 */

	@Override
	public boolean write(Review review) {
		logger.info(">>>>>>>>>> WRITE JDBC start");
		Connection conn = null;
		PreparedStatement pstmt = null;
		int createdCount = 0;
		//Nail nails = new Nail();
		
		//logger.info(">>>>>>>>>> review : " + review.getNailShop().getName());
		
		try {
			
			conn = connectionFactory.createConnection();

			pstmt = conn.prepareStatement("INSERT INTO REVIEW(ID, CONTENT, IMAGE) VALUES (SEQ_REVIEW.NEXTVAL, ?, ?)");
			
			
			pstmt.setString(1,review.getContent());
			pstmt.setString(2,review.getImage());
			
			createdCount = pstmt.executeUpdate();
	
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
		}
		return createdCount > 0 ; 
	}

	
	@Override
	public Review read(String userId) {
		logger.info(">>>>>>>>>> WRITE JDBC start");
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Review review = null;
		NailShop shop = new NailShop();
		Nail nails = new Nail();
		
		logger.info(">>>>>>>>>> review : " + review.getNailShop().getName());
		
		try {
			
			conn = connectionFactory.createConnection();

			pstmt = conn.prepareStatement("SELECT ID, REVIEW_DATE, CONTENT, IMAGE, SHOP_ID, USER_ID, NAIL_ID FROM REVIEW WHERE USER_ID");
			
			rs=pstmt.executeQuery();
			
			//pstmt.setString(1,userId);
			review = new Review();
	
			review.setId(rs.getInt(1));
			review.setDate(rs.getDate(2));
			review.setContent(rs.getString(3));
			review.setImage(rs.getString(4));
			shop.setName(rs.getString(5));
			review.setUserId(rs.getString(6));
			nails.setId(rs.getInt(7));
	
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
		}
		return review; 
	}

	@Override
	public String getUserId() {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		User user= new User();
		
		try {
			
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID FROM USER_TB");
			
			rs=pstmt.executeQuery();
			while (rs.next()) {
				user.setId(rs.getString(1));
	         }
		
			logger.info(">>>>>>>>>>>> user.getId() : " + user.getId());
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
		}
		return user.getId(); 
	}
	
	public Reservation find(String userId) { // 占쎌굙占쎈튋 占쎌젟癰귨옙 筌≪뼐由�
	      String sql = "SELECT ID, RESERVATION_DATE, TIME, DESIGNER FROM RESERVATION WHERE USER_ID = ?";

	      Connection conn = null;
	      PreparedStatement psmt = null;
	      ResultSet rs = null;
	      Reservation reservation = null;

	      try {
	         conn = connectionFactory.createConnection();
	         psmt = conn.prepareStatement(sql.toString());
	         psmt.setString(1, userId);
	         rs = psmt.executeQuery();
	         if (rs.next()) {
	            reservation = new Reservation();
	            reservation.setUserId(rs.getString(1));
	            reservation.setDate(rs.getDate(2));
	            reservation.setTime(rs.getString(3));
	            reservation.setDesigner(rs.getString(4));
	         }
	      } catch (SQLException e) {
	         throw new RuntimeException(e);
	      } finally {
	         JdbcUtils.close(rs, psmt, conn);
	      }

	      return reservation;

	   }

	@Override
	public String getShopId() {
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		NailShop shop = new NailShop();
		
		try {
			
			conn = connectionFactory.createConnection();
			pstmt = conn.prepareStatement("SELECT ID FROM NAILSHOP");
			
			rs=pstmt.executeQuery();
			while (rs.next()) {
				shop.setId(rs.getString(1));
	         }
		
			logger.info(">>>>>>>>>>>> shop.getId() : " + shop.getId());
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(pstmt, conn);
		}
		return shop.getId(); 
	}
	@Override
	   public Nail findNailName(String userId) {
	      String sql = "SELECT NAIL.NAME, NAIL.SHOP_ID FROM NAIL, RESERVATION WHERE RESERVATION.USER_ID = ?";

	      Connection conn = null;
	      PreparedStatement psmt = null;
	      ResultSet rs = null;
	      Nail nail = null;

	      try {
	         conn = connectionFactory.createConnection();
	         psmt = conn.prepareStatement(sql.toString());
	         psmt.setString(1, userId);
	         rs = psmt.executeQuery();
	         if (rs.next()) {
	            nail = new Nail();
	            nail.setName(rs.getString(1));
	            nail.setNailShopName(rs.getString(2));
	            }
	      } catch (SQLException e) {
	         throw new RuntimeException(e);
	      } finally {
	         JdbcUtils.close(rs, psmt, conn);
	      }

	      return nail;
	}
}




