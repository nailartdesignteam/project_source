package com.bu.linc.dao.reservation;


import java.sql.Date;
import java.util.List;


import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;

public interface ReservationDao {
   
   
   boolean insert(String shopId, String userId, int nailId);
   Reservation find(String userId);
   boolean delete(String Id, String userId);
   List<Reservation> readAll();
   List<NailShop> findDate(String date, String time);
   List<NailShop> findById(String id);
   List<Nail> findByShopId(String id);
   String findUserId();
   List<Nail> findByNailId(int nailId);
   
}