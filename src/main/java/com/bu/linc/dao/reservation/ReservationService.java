package com.bu.linc.dao.reservation;

import com.bu.linc.domain.Reservation;

public interface ReservationService {

	void registerReservation(Reservation reservation);
	Reservation findReservation(String reservationId);
	void removeReservation(String Id, String reservationId);
	
}
