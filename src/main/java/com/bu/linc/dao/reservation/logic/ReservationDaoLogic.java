<<<<<<< HEAD
package com.bu.linc.dao.reservation.logic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.reservation.ReservationDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.User;

@Repository
public class ReservationDaoLogic implements ReservationDao {

   private Logger logger = Logger.getLogger(getClass());

   private ConnectionFactory connectionFactory;

   public ReservationDaoLogic() {
      connectionFactory = ConnectionFactory.getInstance();
   }

   @Override
   public boolean insert(String shopId, String userId, int nailId) { // �삁�빟 �젙蹂� ���옣
      Connection connection = null;
      PreparedStatement psmt = null;
      int createdCount = 0;

      try {
         connection = connectionFactory.createConnection();

         psmt = connection.prepareStatement(
               "insert into reservation (ID,RESERVATION_DATE, TIME, DESIGNER, SHOP_ID, NAIL_ID, USER_ID)\r\n" + 
               "SELECT  SEQ_RESERVATION.NEXTVAL, NAILSHOP.POSSIBLE_DATE, NAILSHOP.POSSIBLE_TIME, NAILSHOP.DESIGNER, NAILSHOP.ID, NAIL.ID, USER_TB.ID\r\n" + 
               "FROM NAILSHOP,NAIL,USER_TB\r\n" + 
               "WHERE nailshop.ID = ? and USER_TB.ID = ? and NAIL.ID = ?");
         psmt.setString(1, shopId);
         psmt.setString(2, userId);
         psmt.setInt(3, nailId);
         
         System.out.println("Dao Logic INSERT Success");

         createdCount = psmt.executeUpdate();

      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(psmt, connection);
      }
      return createdCount > 0;
   }

   @Override
   public Reservation find(String userId) { // �삁�빟 �젙蹂� 李얘린
      String sql = "SELECT ID, RESERVATION_DATE, TIME, DESIGNER FROM RESERVATION WHERE USER_ID = ?";

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      Reservation reservation = null;

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, userId);
         rs = psmt.executeQuery();
         if (rs.next()) {
            reservation = new Reservation();
            reservation.setUserId(rs.getString(1));
            reservation.setDate(rs.getDate(2));
            reservation.setTime(rs.getString(3));
            reservation.setDesigner(rs.getString(4));
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return reservation;

   }

   @Override
   public boolean delete(String Id, String userId) { // �삁�빟 �젙蹂� �궘�젣
      Connection connection = null;
      PreparedStatement psmt = null;
      int deleteCount = 0;
      try {
         connection = connectionFactory.createConnection();

         psmt = connection.prepareStatement("DELETE FROM RESERVATION WHERE ID = ? AND USER_ID = ?");
         psmt.setString(1, Id);
         psmt.setString(2, userId);

         deleteCount = psmt.executeUpdate();

      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(psmt, connection);
      }

      return deleteCount > 0;
   }

   @Override
   public List<Reservation> readAll() {
      String sql = "SELECT ID, RESERVATION_DATE, TIME, DESIGNER, SHOP_ID, USER_ID, NAIL_ID FROM RESERVATION";

      Connection conn = null;
      Statement statement = null;
      ResultSet rs = null;
      List<Reservation> reservations = new ArrayList<Reservation>();

      try {
         conn = connectionFactory.createConnection();
         statement = conn.createStatement();
         rs = statement.executeQuery(sql);
         while (rs.next()) {
            Reservation reservation = new Reservation();
            reservation.setId(rs.getInt(1));
            reservation.setDate(rs.getDate(2));
            reservation.setTime(rs.getString(3));
            reservation.setDesigner(rs.getString(4));
            reservation.setNailshopId(rs.getString(5));
            reservation.setUserId(rs.getString(6));
            reservation.setNailId(rs.getInt(7));
            reservations.add(reservation);

         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, statement, conn);
      }

      return reservations;
   }

   @Override
   public List<NailShop> findDate(String date, String time) {
      String sql = "SELECT ID, PASSWORD, NAME, PHONE, EMAIL, ADDRESS, TIME, CONTENT, DESIGNER FROM NAILSHOP WHERE POSSIBLE_DATE = TO_DATE(?,'yyyy-mm-dd') AND POSSIBLE_TIME LIKE ?";
      logger.info(">>>>>>>>>>>>>> date : " + date);
      logger.info(">>>>>>>>>>>>>> time : " + time);
      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<NailShop> nailshops = new ArrayList<NailShop>();

      /*
       * java.sql.Date realDate = java.sql.Date.valueOf(date");
       * 
       * logger.info(">>>>>>>>>>>>>> realDate : " + realDate.toString());
       */
      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
//         psmt.setDate(parameterIndex, x);
//         (1,"%"+date+"%");
         psmt.setString(1, date);
         psmt.setString(2, "%"+time+"%");
         rs = psmt.executeQuery();

         while (rs.next()) {
            NailShop nailshop = new NailShop();
            nailshop.setId(rs.getString(1));
            nailshop.setPassword(rs.getString(2));
            nailshop.setName(rs.getString(3));
            nailshop.setPhone(rs.getString(4));
            nailshop.setEmail(rs.getString(5));
            nailshop.setAddress(rs.getString(6));
            nailshop.setTime(rs.getString(7));
            nailshop.setContent(rs.getString(8));
            nailshop.setDesigner(rs.getString(9));
            nailshops.add(nailshop);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return nailshops;
   }

   @Override
   public List<NailShop> findById(String id) {
      String sql = "SELECT ID, PASSWORD, NAME, PHONE, EMAIL, ADDRESS, TIME, CONTENT FROM NAILSHOP WHERE ID = ?";

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<NailShop> shops = new ArrayList<NailShop>();
      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         if (rs.next()) {
            NailShop shop = new NailShop();
            shop.setId(rs.getString(1));
            shop.setPassword(rs.getString(2));
            shop.setName(rs.getString(3));
            shop.setPhone(rs.getString(4));
            shop.setEmail(rs.getString(5));
            shop.setAddress(rs.getString(6));
            shop.setTime(rs.getString(7));
            shop.setContent(rs.getString(8));
            shops.add(shop);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return shops;
   }

   @Override
   public List<Nail> findByShopId(String id) {
      String sql = "SELECT ID,  NAME, CONTENT, PRICE, IMAGE, SHOP_ID FROM NAIL WHERE SHOP_ID = ?";

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<Nail> nails = new ArrayList<Nail>();

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         while (rs.next()) {
            Nail nail = new Nail();
            nail.setId(rs.getInt(1));
            nail.setName(rs.getString(2));
            nail.setContent(rs.getString(3));
            nail.setPrice(rs.getString(4));
            nail.setImage(rs.getString(5));
            nail.setNailShopName(rs.getString(6));
            nails.add(nail);   
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return nails;
   }

   @Override
   public String findUserId() {
      
      String sql = "SELECT ID FROM USER_TB";
      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      User userId = null;
      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         rs = psmt.executeQuery();
         while (rs.next()) {
            userId = new User();
            userId.setId(rs.getString(1));
            
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return userId.getId();
   }

   @Override
   public List<Nail> findByNailId(int nailId) {
      
      String sql = "SELECT ID,  NAME, CONTENT, PRICE, IMAGE, SHOP_ID FROM NAIL WHERE ID= ?";

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<Nail> nails = new ArrayList<Nail>();
      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setInt(1, nailId);
         rs = psmt.executeQuery();
         if (rs.next()) {
            Nail nail = new Nail();
            nail.setId(rs.getInt(1));
            nail.setName(rs.getString(2));
            nail.setContent(rs.getString(3));
            nail.setPrice(rs.getString(4));
            nail.setImage(rs.getString(5));
            nail.setNailShopName(rs.getString(6));
            nails.add(nail);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return nails;
   }
}

=======
package com.bu.linc.dao.reservation.logic;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.dao.reservation.ReservationDao;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
<<<<<<< HEAD



=======
<<<<<<< HEAD
=======
import com.bu.linc.domain.User;
>>>>>>> branch 'master' of https://gitlab.com/nailartdesignteam/project_source.git
>>>>>>> branch 'master' of https://gitlab.com/nailartdesignteam/project_source.git

@Repository
public class ReservationDaoLogic implements ReservationDao {

	private Logger logger = Logger.getLogger(getClass());

	private ConnectionFactory connectionFactory;

	public ReservationDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean insert(String shopId, String userId, String nailId) { // �삁�빟 �젙蹂� ���옣
		Connection connection = null;
		PreparedStatement psmt = null;
		int createdCount = 0;

		try {
			connection = connectionFactory.createConnection();

			psmt = connection.prepareStatement(
					"insert into reservation (ID,RESERVATION_DATE, TIME, DESIGNER, SHOP_ID, NAIL_ID, USER_ID)\r\n" + 
					"SELECT  SEQ_RESERVATION.NEXTVAL, NAILSHOP.POSSIBLE_DATE, NAILSHOP.POSSIBLE_TIME, NAILSHOP.DESIGNER, NAILSHOP.ID, NAIL.ID, USER_TB.ID\r\n" + 
					"FROM NAILSHOP,NAIL,USER_TB\r\n" + 
					"WHERE nailshop.ID = ? and USER_TB.ID = ? and NAIL.ID = ?");
			psmt.setInt(1, reservation.getId());
			psmt.setDate(2, reservation.getDate());
			psmt.setString(3, reservation.getTime());
			psmt.setString(4, reservation.getDesigner());
			psmt.setString(5, reservation.getUserId());
			psmt.setInt(6, reservation.getNailId());
			psmt.setString(7, reservation.getNailshopId());

			System.out.println("Dao Logic INSERT Success");

			createdCount = psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}
		return createdCount > 0;
	}

	@Override
	public Reservation find(String userId) { // �삁�빟 �젙蹂� 李얘린
		String sql = "SELECT ID, RESERVATION_DATE, TIME, DESIGNER FROM RESERVATION WHERE USER_ID = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		Reservation reservation = null;

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, userId);
			rs = psmt.executeQuery();
			if (rs.next()) {
				reservation = new Reservation();
				reservation.setUserId(rs.getString(1));
				reservation.setDate(rs.getDate(2));
				reservation.setTime(rs.getString(3));
				reservation.setDesigner(rs.getString(4));
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return reservation;

	}

	@Override
	public boolean delete(String Id, String userId) { // �삁�빟 �젙蹂� �궘�젣
		Connection connection = null;
		PreparedStatement psmt = null;
		int deleteCount = 0;
		try {
			connection = connectionFactory.createConnection();

			psmt = connection.prepareStatement("DELETE FROM RESERVATION WHERE ID = ? AND USER_ID = ?");
			psmt.setString(1, Id);
			psmt.setString(2, userId);

			deleteCount = psmt.executeUpdate();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(psmt, connection);
		}

		return deleteCount > 0;
	}

	@Override
	public List<Reservation> readAll() {
		String sql = "SELECT ID, RESERVATION_DATE, TIME, DESIGNER, SHOP_ID, USER_ID, NAIL_ID FROM RESERVATION";

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<Reservation> reservations = new ArrayList<Reservation>();

		try {
			conn = connectionFactory.createConnection();
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			while (rs.next()) {
				Reservation reservation = new Reservation();
				reservation.setId(rs.getInt(1));
				reservation.setDate(rs.getDate(2));
				reservation.setTime(rs.getString(3));
				reservation.setDesigner(rs.getString(4));
				reservation.setNailshopId(rs.getString(5));
				reservation.setUserId(rs.getString(6));
				reservation.setNailId(rs.getInt(7));
				reservations.add(reservation);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, statement, conn);
		}

		return reservations;
	}

	@Override
	public List<NailShop> findDate(String date, String time) {
		String sql = "SELECT ID, PASSWORD, NAME, PHONE, EMAIL, ADDRESS, TIME, CONTENT, DESIGNER FROM NAILSHOP WHERE POSSIBLE_DATE = TO_DATE(?,'yyyy-mm-dd') AND POSSIBLE_TIME LIKE ?";
		logger.info(">>>>>>>>>>>>>> date : " + date);
		logger.info(">>>>>>>>>>>>>> time : " + time);
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<NailShop> nailshops = new ArrayList<NailShop>();

		/*
		 * java.sql.Date realDate = java.sql.Date.valueOf(date");
		 * 
		 * logger.info(">>>>>>>>>>>>>> realDate : " + realDate.toString());
		 */
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
//			psmt.setDate(parameterIndex, x);
//			(1,"%"+date+"%");
			psmt.setString(1, date);
			psmt.setString(2, "%"+time+"%");
			rs = psmt.executeQuery();

			while (rs.next()) {
				NailShop nailshop = new NailShop();
				nailshop.setId(rs.getString(1));
				nailshop.setPassword(rs.getString(2));
				nailshop.setName(rs.getString(3));
				nailshop.setPhone(rs.getString(4));
				nailshop.setEmail(rs.getString(5));
				nailshop.setAddress(rs.getString(6));
				nailshop.setTime(rs.getString(7));
				nailshop.setContent(rs.getString(8));
				nailshop.setDesigner(rs.getString(9));
				nailshops.add(nailshop);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return nailshops;
	}

	@Override
	public List<NailShop> findById(String id) {
		String sql = "SELECT ID, PASSWORD, NAME, PHONE, EMAIL, ADDRESS, TIME, CONTENT FROM NAILSHOP WHERE ID = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<NailShop> shops = new ArrayList<NailShop>();
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			if (rs.next()) {
				NailShop shop = new NailShop();
				shop.setId(rs.getString(1));
				shop.setPassword(rs.getString(2));
				shop.setName(rs.getString(3));
				shop.setPhone(rs.getString(4));
				shop.setEmail(rs.getString(5));
				shop.setAddress(rs.getString(6));
				shop.setTime(rs.getString(7));
				shop.setContent(rs.getString(8));
				shops.add(shop);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return shops;
	}

	@Override
	public List<Nail> findByShopId(String id) {
		String sql = "SELECT ID,  NAME, CONTENT, PRICE, IMAGE, SHOP_ID FROM NAIL WHERE SHOP_ID = ?";

		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		List<Nail> nails = new ArrayList<Nail>();

		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			psmt.setString(1, id);
			rs = psmt.executeQuery();
			while (rs.next()) {
				Nail nail = new Nail();
				nail.setId(rs.getInt(1));
				nail.setName(rs.getString(2));
				nail.setContent(rs.getString(3));
				nail.setPrice(rs.getString(4));
				nail.setImage(rs.getString(5));
				nail.setNailShopName(rs.getString(6));
				nails.add(nail);	
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return nails;
	}

	/*@Override
	public String findUserId() {
		
		String sql = "SELECT ID FROM USER_TB";
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		User userId = null;
		try {
			conn = connectionFactory.createConnection();
			psmt = conn.prepareStatement(sql.toString());
			rs = psmt.executeQuery();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getString(1));
				
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtils.close(rs, psmt, conn);
		}

		return user;*/
	}

}
>>>>>>> branch 'master' of https://gitlab.com/nailartdesignteam/project_source.git
