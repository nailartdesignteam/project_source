package com.bu.linc.dao.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.WishListDao;
import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.WishList;

@Repository
public class WishListDaoLogic implements WishListDao{
	
	private Logger logger = Logger.getLogger(getClass());
	
	private ConnectionFactory connectionFactory;
	
	public WishListDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}

	@Override
	public boolean add(String userId, int nailId) {
		logger.info(">>>>>>>>>>>>>>>>> DaoLogic userId : " + userId);
		logger.info(">>>>>>>>>>>>>>>>> DaoLogic nailId : " + nailId);
		
		Connection con = null;
		PreparedStatement pstmt = null;
//		NailShop shop = null;
//		Nail nail = null;
//		WishList wishList = null;
//		shop = new NailShop();
//        nail = new Nail();
//        wishList = new WishList();
		int createdCount = 0;
		
		try {
			logger.info(">>>>>>>>>>>>>>>>> DaoLogic try complete");
			logger.info(">>>>>>>>>>>>>>>>> DaoLogic try userId : " + userId);
			logger.info(">>>>>>>>>>>>>>>>> DaoLogic try nailId : " + nailId);
			
			con = connectionFactory.createConnection();
			
			pstmt = con.prepareStatement("INSERT INTO WISHLIST (ID, USER_ID, NAIL_ID) VALUES (SEQ_WISHLIST.NEXTVAL, ?, ?)");

			pstmt.setString(1, "duswo");
			pstmt.setInt(2, nailId);
			
			createdCount = pstmt.executeUpdate();
			System.out.println("QUERY >> " + pstmt.toString());
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(pstmt, con);
			logger.info(">>>>>>>>>>>>>>>>> DaoLogic JdbcUtiles Close complete");
		}
		return createdCount > 0;
	}

	@Override
	public void delete(int id) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = connectionFactory.createConnection();
			
			pstmt = con.prepareStatement("DELETE FROM WISHLIST WHERE ID = ?");
			
			pstmt.setInt(1, id);
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(pstmt, con);
		}
		
	}

	@Override
	public List<Nail> findAll(String userId, String nailId) {
		String sql = "SELECT NAIL.ID, NAIL.NAME, NAIL.CONTENT, NAIL.IMAGE, NAIL.PRICE "
				+ "FROM WISHLIST, NAIL "
				+ "WHERE WISHLIST.NAIL_ID = ? "
				+ "AND WISHLIST.USER_ID = ? ";

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<Nail> nails = new ArrayList<Nail>();
		Nail nail = null;
		
		try {
			con = connectionFactory.createConnection();
			pstmt = con.prepareStatement(sql.toString());
			
			pstmt.setString(1, nailId);
			pstmt.setString(2, userId);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				
				nail = new Nail();
	            nail.setId(rs.getInt(1));
	            nail.setName(rs.getString(2));
	            nail.setContent(rs.getString(3));
	            nail.setImage(rs.getString(4));
	            nail.setPrice(rs.getString(5));
	            nails.add(nail);
			}
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(rs, pstmt, con);
		}
		return nails;
	}

	@Override
	public List<Nail> findNail(int nailId) {
		String sql = "SELECT NAIL.ID, NAILSHOP.NAME, NAIL.NAME, NAIL.IMAGE, NAIL.CONTENT, NAIL.PRICE FROM NAIL, NAILSHOP WHERE NAIL.SHOP_ID = NAILSHOP.ID AND NAIL.ID=?";
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<Nail> nails = new ArrayList<Nail>();
		Nail nail = null;
		
		try {
			con = connectionFactory.createConnection();
			pstmt = con.prepareStatement(sql.toString());
			
			pstmt.setInt(1, nailId);
			rs = pstmt.executeQuery();
			while(rs.next()) {

	            nail = new Nail();
	            nail.setId(rs.getInt(1));
				nail.setNailShopName(rs.getString(2));
				nail.setName(rs.getString(3));
				nail.setImage(rs.getString(4));
				nail.setContent(rs.getString(5));
				nail.setPrice(rs.getString(6));
	            nails.add(nail);
			}
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			JdbcUtils.close(rs, pstmt, con);
		}
		return nails;
	}
	
}
