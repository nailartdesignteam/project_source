package com.bu.linc.dao.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.ShopDao;
import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;

@Repository
public class ShopDaoLogic implements ShopDao{

   private ConnectionFactory connectionFactory;
   
   public ShopDaoLogic() {
      connectionFactory = ConnectionFactory.getInstance();
   }

   @Override
   public List<NailShop> readAllShop() {      // 네일샵 전체 목록
      
      String sql = "SELECT ID, NAME, CONTENT, PHONE, EMAIL, ADDRESS, TIME FROM NAILSHOP" ;

      Connection conn = null;
      Statement statement = null;
      ResultSet rs = null;
      List<NailShop> shops = new ArrayList<NailShop>();

      try {
         conn = connectionFactory.createConnection();
         statement = conn.createStatement();
         rs = statement.executeQuery(sql);
         while (rs.next()) {
            NailShop shop = new NailShop();
            shop.setId(rs.getString(1));
            shop.setName(rs.getString(2));
            shop.setContent(rs.getString(3));
            shop.setPhone(rs.getString(4));
            shop.setEmail(rs.getString(5));
            shop.setAddress(rs.getString(6));
            shop.setTime(rs.getString(7));
            shops.add(shop);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, statement, conn);
      }

      return shops;
   }

   @Override
   public List<Nail> findById(String id) {      // 각 네일샵에 해당하는 네일 목록
      String sql = "SELECT NAIL.IMAGE, NAIL.NAME, NAIL.PRICE, NAIL.SHOP_ID FROM NAIL WHERE NAIL.SHOP_ID = ?" ;
      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<Nail> nails = new ArrayList<Nail>();

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         
         while (rs.next()) {
            Nail nail = new Nail();
            nail.setImage(rs.getString(1));
            nail.setName(rs.getString(2));
            nail.setPrice(rs.getString(3));
            nail.setNailShopName(rs.getString(4));
            nails.add(nail);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return nails;
   }
   
   @Override
   public List<NailShop> shopInfo(String id) {      // 네일샵 (이름, 영업시간, 연락처, 이메일, 주소 등)의 정보 
      String sql = "SELECT NAME, TIME, PHONE, EMAIL, ADDRESS, MAP_ADDRESS FROM NAILSHOP WHERE ID = ?" ;
      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      List<NailShop> shops = new ArrayList<NailShop>();

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         
         if (rs.next()) {
            NailShop shop = new NailShop();
            shop.setName(rs.getString(1));
            shop.setTime(rs.getString(2));
            shop.setPhone(rs.getString(3));
            shop.setEmail(rs.getString(4));
            shop.setAddress(rs.getString(5));
            shop.setMap_address(rs.getString(6));
            shops.add(shop);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return shops;
   }
   

   @Override
   public NailShop read(String id) {  // 네일샵
      
      String sql = "SELECT ID, NAME, CONTENT, PHONE, EMAIL, ADDRESS, TIME FROM NAILSHOP WHERE ID = ?" ;

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      NailShop shop = null;

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         if (rs.next()) {
            shop = new NailShop();
            shop.setId(rs.getString(1));
            shop.setName(rs.getString(2));
            shop.setContent(rs.getString(3));
            shop.setPhone(rs.getString(4));
            shop.setEmail(rs.getString(5));
            shop.setAddress(rs.getString(6));
            shop.setTime(rs.getString(7));
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return shop;
   }

   @Override
   public Nail readNail(String id) {      // 네일
      // TODO Auto-generated method stub
      String sql = "SELECT ID, IMAGE, NAME, PRICE, SHOP_ID FROM NAILSHOP WHERE SHOP_ID = ?" ;

      Connection conn = null;
      PreparedStatement psmt = null;
      ResultSet rs = null;
      Nail nail = null;

      try {
         conn = connectionFactory.createConnection();
         psmt = conn.prepareStatement(sql.toString());
         psmt.setString(1, id);
         rs = psmt.executeQuery();
         if (rs.next()) {
            nail = new Nail();
            nail.setId(rs.getInt(1));
            nail.setImage(rs.getString(2));
            nail.setName(rs.getString(3));
            nail.setPrice(rs.getString(4));
            nail.setNailShopName(rs.getString(5));
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, psmt, conn);
      }

      return nail;
   }

   @Override
   public List<Nail> readAllNail() {      // 각 네일샵에 해당하는 네일 상품 전체 
      String sql = "SELECT ID, IMAGE, NAME, PRICE, SHOP_ID FROM NAIL" ;

      Connection conn = null;
      Statement statement = null;
      ResultSet rs = null;
      List<Nail> nails = new ArrayList<Nail>();

      try {
         conn = connectionFactory.createConnection();
         statement = conn.createStatement();
         rs = statement.executeQuery(sql);
         while (rs.next()) {
            Nail nail = new Nail();
            nail.setId(rs.getInt(1));
            nail.setImage(rs.getString(2));
            nail.setName(rs.getString(3));
            nail.setPrice(rs.getString(4));
            nail.setNailShopName(rs.getString(5));
            nails.add(nail);
         }
      } catch (SQLException e) {
         throw new RuntimeException(e);
      } finally {
         JdbcUtils.close(rs, statement, conn);
      }
      return nails;
   }
   
}