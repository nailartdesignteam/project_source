package com.bu.linc.dao.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.management.RuntimeErrorException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartRequest;

import com.bu.linc.dao.UploadDao;
import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.domain.FileTest;

@Repository
public class UploadDaoLogic implements UploadDao{

	private Logger logger = Logger.getLogger(getClass());
	
	private ConnectionFactory connectionFactory;
	
	public UploadDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	
	@Override
	public boolean insert(int id, String image, String extension)  {
		Connection connection = null;
		PreparedStatement pstmt = null;
	
		int createdCount = 0;
		
		try {
			connection = connectionFactory.createConnection();
			
			String SQL = "INSERT INTO FILETEST (ID, IMAGE, EXTENSION) VALUES(?, ?, ?)";

			pstmt = connection.prepareStatement(SQL);
			pstmt.setInt(1, id);
			pstmt.setString(2, image);
			pstmt.setString(3, extension);
			
			System.out.println(pstmt.toString());
			
			createdCount = pstmt.executeUpdate();
			logger.info(">>>>>>>>>>>> SQL QUERY executeUpdate Complete");
			
		} catch(SQLException e) {
			throw new RuntimeException(e);
		} finally {
			//resources 해제
			JdbcUtils.close(pstmt,connection);
			logger.info(">>>>>>>>>>>> JDBC RESOURCE close Complete");
		}
		
		return createdCount > 0;
	}

}