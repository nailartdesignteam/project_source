package com.bu.linc.dao.logic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bu.linc.dao.NailDao;
import com.bu.linc.dao.factory.ConnectionFactory;
import com.bu.linc.dao.logic.jdbcutils.JdbcUtils;
import com.bu.linc.domain.Nail;
import com.bu.linc.domain.Review;

@Repository
public class NailDaoLogic implements NailDao{

	private Logger logger = Logger.getLogger(getClass());
	
	private ConnectionFactory connectionFactory;
	public NailDaoLogic() {
		connectionFactory = ConnectionFactory.getInstance();
	}
	
	@Override
	public Nail find(int id) {

		logger.info(">>>>>>>>>>>> DaoLogic try success !!! ��������������������������������");
		logger.info(">>>>>>>>>>>> DaoLogic id : ��������������������������������" + id);
		
		String sql = "SELECT NAIL.ID, NAILSHOP.NAME, NAIL.NAME, NAIL.IMAGE, NAIL.CONTENT, NAIL.PRICE FROM NAIL, NAILSHOP WHERE NAIL.SHOP_ID = NAILSHOP.ID AND NAIL.ID=?";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		Nail nail = new Nail();
		
		try {
			logger.info(">>>>>>>>>>>> try success !!! ��������������������������������");
			
			con = connectionFactory.createConnection();
			pstmt = con.prepareStatement(sql.toString());
			logger.info(">>>>>>>>>>>> DaoLogic id : ��������������������������������" + id);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			int test = id;
			logger.info(">>>>>>>>>>>> DaoLogic test : ��������������������������������" + test);
			logger.info(">>>>>>>>>>>> DaoLogic try success !!! ��������������������������������");
			if (rs.next()) {
				nail.setId(rs.getInt(1));
				nail.setNailShopName(rs.getString(2));
				nail.setName(rs.getString(3));
				nail.setImage(rs.getString(4));
				nail.setContent(rs.getString(5));
				nail.setPrice(rs.getString(6));
			}
			
		}catch(SQLException e) {
			throw new RuntimeException(e);
		}finally {
			logger.info(">>>>>>>>>>>> resource close success !!! ��������������������������������");
			JdbcUtils.close(rs, pstmt, con);
		}
		return nail;
	}

	@Override
	public List<Review> findAll(int id) {
		System.out.println(">>>>>>>>>>>> DaoLogic 3333333333333");
		System.out.println(">>>>>>>>>>>> DaoLogic id" + id);
		
		String sql = "SELECT ID, USER_ID, REVIEW_DATE, CONTENT, IMAGE FROM REVIEW WHERE NAIL_ID = ?" ;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		List<Review> reviews = new ArrayList<Review>();
		Review review = null;
		
		try {
			logger.info(">>>>>>>>>>>> DaoLogic try success !!! 4444444444444444");
		
			con = connectionFactory.createConnection();
			pstmt = con.prepareStatement(sql.toString());
			
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
			
	            review = new Review();
	            review.setId(rs.getInt(1));
	            review.setUserId(rs.getString(2));
	            review.setDate(rs.getDate(3));
	            review.setContent(rs.getString(4));
	            review.setImage(rs.getString(5));
	            reviews.add(review);

			}
			
		}catch(SQLException e) {
			logger.info(">>>>>>>>>>>> DaoLogic catch !!! 5555555555555555555");
			throw new RuntimeException(e);
		}finally {
			logger.info(">>>>>>>>>>>> DaoLogic finally !!! 66666666666666666666666");
			JdbcUtils.close(rs, pstmt, con);
			logger.info(">>>>>>>>>>>> DaoLogic resources closd success !!! 7777777777777777777");
		}
		return reviews;
	}

}
