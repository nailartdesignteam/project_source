package com.bu.linc.controller.nail;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.Review;
import com.bu.linc.service.NailService;

@Controller
public class NailController {
	
	@Autowired
	private NailService service;
	
//	@RequestMapping(value="/nailDetail", method=RequestMethod.POST)
//	public String nailDetail(int id, Model model) {
//		Nail nail = service.findNail(id);
//		model.addAttribute("nail", nail);
//		return "product/productDetail?nailId="+nail.getId();
//	}
//	
	
	@RequestMapping("/nailDetail/review")
	public String list(int id, Model model) {		
		List<Review> reviews = service.findAllReviews(id);
		model.addAttribute("reviews", reviews);
		return "product/productDetail";
	}
	
	@RequestMapping("/nailDetail")
	public String productDetail(int id, Model model) {
		Nail nail = service.findNail(id);
		model.addAttribute("nail", nail);
		return "product/productDetail";
	}
}
