package com.bu.linc.controller.review;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.domain.Review;
import com.bu.linc.service.ReviewService;

@Controller
public class ReviewController {
	
	
	@Autowired
	private ReviewService service;
	
	
	@RequestMapping("/myReservation")
	public String findReservation(String userId, Model model) {
		
		Reservation reservation = service.findAllReservation(userId);
		model.addAttribute("reservation",reservation);
		
		Nail nail = service.findNailName(userId);
		model.addAttribute("nail", nail);
		
		return "mypage/reservationHistory";
		
	}
	
	 @RequestMapping("/myPage") 
	 
	 public String getId(Model model) {
	 
		 String userId = service.findUserId(); 
		 String shopId = service.findShopId();
		 model.addAttribute("userId",userId);
		 model.addAttribute("shopId",shopId);
		 
		 return "mypage/mypage"; 
	 }
	 
	
    @RequestMapping("/myReview") 
    public String findReview(String shopId, String userId, Model model) { 
    
    	
    	List<Review> reviews = service.findAllReviews();
    	model.addAttribute("reviews", reviews);
    	
    	List<NailShop> shops = service.findName(shopId, userId);
    	model.addAttribute("shops",shops);
    	
    	//NailShop shopName = service.readName();
    	
    	//ModelAndView modelAndView = new ModelAndView("mypage/review");
    	//modelAndView.addObject("reviewList",reviews);
    	
    	return "mypage/review";
    	
	}
	
	@RequestMapping(value="/reviewWrite", method=RequestMethod.GET)
	public String regist(Model model) {
		
		//List<Review> review = service.findAllReviews();
		//model.addAttribute("review", review);
		return "review/reviewForm";
		
	}
	
	 @RequestMapping(value="/product", method=RequestMethod.POST)
	 public String regist(Review review, HttpSession session, Model model,String userId) {
		 
		 List<Review> reviews = service.findAllReviews();
		 model.addAttribute("reviews",reviews);
		 //review.setUserId((String)session.getAttribute("name"));
		 service.registerReview(review);
		 service.read(userId);

		 return "product/productDetail"; 
		 
	 }


}
