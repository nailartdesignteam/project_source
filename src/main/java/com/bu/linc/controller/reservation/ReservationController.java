package com.bu.linc.controller.reservation;

import java.sql.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.Reservation;
import com.bu.linc.service.ReservationService;


@Controller
public class ReservationController {
   
   private Logger logger = Logger.getLogger(getClass());
   
   @Autowired
   private ReservationService service;
   
   @RequestMapping(value="/reservation1")
   public String reservation1() {
      return "reservation/reservation_1";
   }
   
   @RequestMapping(value="/reservation2")
   public String reservation2() {
      return "reservation/reservation_2";
   }
   @RequestMapping(value="/reservation3")
   public String reservation3() {
      return "reservation/reservation_3";
   }
   
   @RequestMapping(value="/search",method = RequestMethod.GET)
   public String search(String date, String time, Model model) {
      logger.info(">>>>>>>>>>  Controller  Mapping /serach complete");
      logger.info(">>>>>>>>>>  Controller  date: " + date);
      logger.info(">>>>>>>>>>  Controller  time: " + time);
      
      List<NailShop> shops = service.findNailshop(date,time);
      logger.info(">>>>>>>>>>   service findNailShop complete");
      
      for(NailShop data : shops) {
         System.out.println(data);
      }
      
      System.out.println(shops.toString());
      logger.info("�� ��� �Ϸ�");
      
      model.addAttribute("shops", shops);
      if(shops.isEmpty()) {
         System.out.println("���� ������ϴ�.");
      }
      Iterator<NailShop> empIterator = shops.iterator();
      
      while (empIterator.hasNext()) {
         
          logger.info(empIterator.next());
      }
      return "reservation/reservation_1";
   }
   @RequestMapping(value="/select" ,method = RequestMethod.GET)
   public String select(String shopId, Model model) {
      logger.info(shopId);
      List<NailShop> shops = service.findByShopId(shopId);
      model.addAttribute("shop", shops);
      
      List<Nail> nails = service.findNail(shopId);
      model.addAttribute("nails",nails);
      String userId = service.findUserId();
      model.addAttribute("userId",userId);
      
      return "reservation/reservation_2";
   }
   @RequestMapping(value="/image")
   public String select(int nailId, String shopId, String userId, Model model) {
      service.registReservation(shopId, userId, nailId);
      List<NailShop> shops = service.findByShopId(shopId);
      model.addAttribute("shop",shops);
      List<Nail> nails = service.findByNailId(nailId);
      model.addAttribute("nail",nails);
      Reservation reservation = service.findReservation(userId);
      model.addAttribute("reservation",reservation);
      return "reservation/reservation_3";
   }
   
   @RequestMapping(value="/lookup")
   public String lookup(String userId, String shopId,Model model) {
      Reservation reservation = service.findReservation(userId);
      model.addAttribute("reservation",reservation);
      List<Nail> nails = service.findNail(shopId);
      model.addAttribute("nail",nails);
      List<NailShop> shops = service.findByShopId(shopId);
      model.addAttribute("shop",shops);
      return "reservation/reservation_3";
      
   }
}