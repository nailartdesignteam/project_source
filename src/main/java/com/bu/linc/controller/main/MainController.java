package com.bu.linc.controller.main;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.service.MainService;

@Controller
public class MainController {
   
   private Logger logger = Logger.getLogger(getClass());
   
   @Autowired
   private MainService service;
  
   @RequestMapping("main/main-page")
   public String page() {
      return "main/main-page";
   }
   @RequestMapping("main/main-page2")
   public String page2() {
      return "main/main-page2";
   }
   //메인페이지
   @RequestMapping(value="/index")
   public ModelAndView main() {   

      List<Nail> nails = service.readAll();
      
      ModelAndView modelAndView = new ModelAndView("main/main-page");
      modelAndView.addObject("nailList", nails);
      
      return modelAndView;
   }
   @RequestMapping("/mainSearch")
   public String mainSearch(String shop,String designer, String date, String time,Model model) {
      List<NailShop> shops = service.mainSearch(shop, designer, date, time);
      model.addAttribute("shops",shops);
      
      return "main/main-page2";
   }
   
}
