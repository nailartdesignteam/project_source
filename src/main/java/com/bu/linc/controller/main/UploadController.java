package com.bu.linc.controller.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bu.linc.service.UploadService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class UploadController {
	 
	private static Logger logger = LoggerFactory.getLogger(UploadController.class);
	
	@Autowired
	private UploadService service;
	
	 //mvc-config.xml�뿉 �꽕�젙�맂 由ъ냼�뒪 李몄“�븯湲� �쐞�븿
	 //bean�쓽 id媛� uploadPath�씤 �깭洹몃�� 李몄“
	 
	 @Resource(name = "uploadPath")
	 String uploadPath;
	 //uploadPath瑜� workspace �궡遺� �뤃�뜑 寃쎈줈�뒗 �궗�엺留덈떎 �떎�떎瑜몃뜲 �뼱�뼸寃� �넻�씪?
	 //�씠嫄� �빐寃고븯硫� �걹�씤媛�???
	 
	 
	 @RequestMapping(value="/upload/uploadForm", method=RequestMethod.GET)
	 public void uploadForm() {
//		 return "upload/uploadForm"; // upload/uploadForm.jsp �럹�씠吏� �룷�썙�뵫
	 }
	 
	 //�떎以묓뙆�씪�뾽濡쒕뱶
	 //�뾽濡쒕뱶 踰꾪듉 => �엫�떆�뵒�젆�넗由ъ뿉 �뾽濡쒕뱶
	 //=> �뙆�씪�젙蹂닿� file�뿉 ���옣
	 //=> 吏��젙�맂 �뵒�젆�넗由ъ뿉 ���옣 =>
	@RequestMapping(value="/upload/uploadForm", method=RequestMethod.POST)
	public ModelAndView uploadForm(HttpServletRequest request, MultipartFile file, ModelAndView mav) throws Exception{
		
		String savedName = file.getOriginalFilename();		 				
		
		logger.info("�뙆�씪 �씠由� : " + file.getOriginalFilename());
		logger.info("�뙆�씪 �겕湲� : " + file.getSize());
		logger.info("而⑦뀗�듃 ���엯 : " + file.getContentType());
		// 1. �뙆�씪紐�
		// 2. lastIndex
		// 3. fileName - subString
		// 4. extension - subString
		
//		String path = request.getSession().getServletContext().getRealPath("/resources/images/yong");
//		logger.info(">>>>>>>>>>>> PATH TEST : " + path);
//		String path2 = path + "resources" + File.separator + "images" + File.separator + "yong" + File.separator;
//		logger.info(">>>>>>>>>>>> PATH2 TEST : " + path2);
		 
		savedName = uploadFile(savedName, file.getBytes());
		logger.info(">>>>>>>>>>>>> savedName 111111-----11111 : " + savedName);
		int lastIndex = savedName.lastIndexOf('.');
		String savedName_Name = savedName.substring(0, lastIndex);
		String extension = savedName.substring(lastIndex +1);
		logger.info(">>>>>>>>>>>>> savedName 111111 : " + savedName_Name);
		logger.info(">>>>>>>>>>>>> extension 111111 : " + extension);
				
		mav.setViewName("upload/uploadResult");	//setViewName = �쓳�떟�븷 view�쓽 �씠由꾩쓣 �꽕�젙
		mav.addObject("savedName", savedName);	//addObject = view�뿉 �쟾�떖�븷 媛믪쓣 �꽕�젙
		mav.addObject("extension", extension);
		
		logger.info(">>>>>>>>>>>>> savedName 2222222 : " + savedName);
		logger.info(">>>>>>>>>>>>> extension 2222222 : " + extension);
		
			
		service.insert(7, savedName, extension);
		
		return mav; //�빐�떦媛믪쓣 由ы꽩�븯硫�, uploadResult.jsp �럹�씠吏� �씠�룞
	}
	 
	 //�뙆�씪�씠由꾩씠 以묐났�릺吏� �븡�룄濡� 泥섎━
	 public String uploadFile(String originalName, byte[] fileData) throws Exception {

		 // UUID�겢�옒�뒪�쓽 �궗�슜
		 // 1. �뾽濡쒕뱶�맂 �뙆�씪紐낆쓽 以묐났�쓣 諛⑹�
		 // 2. �씪�젴踰덊샇 ���떊 �쑀異뷀븯湲� �옒�뱺 �떇蹂꾩옄瑜� �씠�슜�븯�뿬 �젒洹� �젣�븳
		 // 3. �뙆�씪�떒�슦濡쒕뱶�떆 �떎瑜� �뙆�씪�쓣 �삁痢≫븯�뿬 �떎�슫濡쒕뱶�븯�뒗 寃� 諛⑹�
		 UUID uid = UUID.randomUUID();
		 String savedName = uid.toString() + "_" + originalName;
		 logger.info(">>>>>>>>>>>> PATH TEST : " + uploadPath);
		 File target = new File(uploadPath, savedName);
		 logger.info(">>>>>>>>>>>> savedName 2222222: " + savedName);
		 
		 // �엫�떆 �뵒�젆�넗由ъ뿉 ���옣�맂 �뾽濡쒕뱶�맂 �뙆�씪�쓣
		 // 吏��젙�맂 �뵒�젆�넗由щ줈 蹂듭궗
		 // FilecopyUtils.copy(諛붿씠�듃諛곗뿴, �뙆�씪媛앹껜)
		 FileCopyUtils.copy(fileData, target);
		 
		 return savedName;
	 }
	 
	 /*
   @RequestMapping(value="/upload/uploadForm", method=RequestMethod.POST)
   public void multiplePhotoUpload(HttpServletRequest request, HttpServletResponse response){
      System.out.println("multiplePhotoUpload");
  
	 
    try {
        //�뙆�씪�젙蹂�
        String sFileInfo = "";
        //�뙆�씪紐낆쓣 諛쏅뒗�떎 - �씪諛� �썝蹂명뙆�씪紐�
        String filename = request.getHeader("file-namxe");
        //�뙆�씪 �솗�옣�옄
        String filename_ext = filename.substring(filename.lastIndexOf(".")+1);
        //�솗�옣�옄瑜쇱냼臾몄옄濡� 蹂�寃�
        filename_ext = filename_ext.toLowerCase();
        //�뙆�씪 湲곕낯寃쎈줈
        String dftFilePath = request.getSession().getServletContext().getRealPath("/");
        logger.info(">>>>>>>>>>>> UploadController _ dftFilePath : " + dftFilePath);
        //�뙆�씪 湲곕낯寃쎈줈 _ �긽�꽭寃쎈줈
        String filePath = dftFilePath + "resource" + File.separator + "images" + File.separator + "yong" + File.separator;
        logger.info(">>>>>>>>>>>> UploadController _ FilePath : " + filePath);
        
        File file = new File(filePath);
        if(!file.exists()) {
           file.mkdirs();
        }
        
        String realFileNm = "";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        logger.info(">>>>>>>>>>>> UploadController _ formatter : " + formatter.toString());
        String today= formatter.format(new java.util.Date());
        logger.info(">>>>>>>>>>>> UploadController _ today : " + today.toString());
        
        logger.info(">>>>>>>>>>>> UploadController _ realFileNm : " + realFileNm.toString());
        

        UUID uid = UUID.randomUUID();
        String testToday = today + uid.toString();
        String testFilename = filename + uid.toString();
//        String testToday = today+UUID.randomUUID().toString();
//        String testFilename = filename.substring(filename.lastIndexOf("."));
        logger.info(">>>>>>>>>>>> UploadController _ testToday : " + testToday.toString());
        logger.info(">>>>>>>>>>>> UploadController _ testFilename : " + testFilename.toString());
             
        realFileNm = today+UUID.randomUUID().toString() + filename.substring(filename.lastIndexOf("."));
        logger.info(">>>>>>>>>>>> UploadController _ realFileNm : " + realFileNm.toString());
        
        String rlFileNm = filePath + realFileNm;
        
        ///////////////// �꽌踰꾩뿉 �뙆�씪�벐湲� /////////////////
        InputStream is = request.getInputStream();
        OutputStream os=new FileOutputStream(rlFileNm);
        int numRead;
        byte b[] = new byte[Integer.parseInt(request.getHeader("file-size"))];
        while((numRead = is.read(b,0,b.length)) != -1){
           os.write(b,0,numRead);
        }
        if(is != null) {
           is.close();
        }
        os.flush();
        os.close();
        ///////////////// �꽌踰꾩뿉 �뙆�씪�벐湲� /////////////////
        
        // �젙蹂� 異쒕젰
        sFileInfo += "&bNewLine=true";
        // img �깭洹몄쓽 title �냽�꽦�쓣 �썝蹂명뙆�씪紐낆쑝濡� �쟻�슜�떆耳쒖＜湲� �쐞�븿
        sFileInfo += "&sFileName="+ filename;;
        sFileInfo += "&sFileURL="+"/resource/images/yong/"+realFileNm;
            PrintWriter print = response.getWriter();
            print.print(sFileInfo);
            print.flush();
            print.close();
       } catch (Exception e) {
           e.printStackTrace();
       }
   }*/
}