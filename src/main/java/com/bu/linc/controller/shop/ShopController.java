package com.bu.linc.controller.shop;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.service.ShopService;

@Controller
public class ShopController {

	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private ShopService service;
	
	@RequestMapping("/shopList")  
	public ModelAndView shopList() {		// 네일샵 목록
		String test = "네일샵 목록";
		List<NailShop> shops = service.findAllShop();
		
		logger.info(">>>>> test " + test);
		logger.info(">>>>> List<NailShop> : " + shops.getClass());
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/shop/shopList");		// 네일샵 목록 페이지
		modelAndView.addObject("shopList", shops);		// 네일샵 목록
		
		return modelAndView;
	}
	
	@RequestMapping(value="/shopMain",method=RequestMethod.GET)
	public ModelAndView shopMain(String id) {		// 네일샵 메인 페이지:네일 상품, 네일샵 정보 
		List<Nail> nails = service.readById(id);
		List<NailShop> shops = service.readShopInfo(id);
		
		logger.info(">>>>> List<Nail> : " + nails.getClass());
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("shop/shopMain");		// 네일샵 메인 페이지
		modelAndView.addObject("nailList", nails);		// design (네일 상품 목록)
		modelAndView.addObject("shopInfo", shops);		// infomation (네일샵 정보)
				
		return modelAndView;
	}
	
}
