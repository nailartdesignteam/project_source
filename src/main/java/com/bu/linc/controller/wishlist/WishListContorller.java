package com.bu.linc.controller.wishlist;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bu.linc.domain.Nail;
import com.bu.linc.domain.NailShop;
import com.bu.linc.domain.WishList;
import com.bu.linc.service.WishListService;

@Controller
public class WishListContorller {
	
	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	private WishListService service;
	
	@RequestMapping(value="/myWishList/add")
	public String add(String userId, int nailId, Model model) {
	
		List<Nail> nails = service.findNailInfo(nailId);
		model.addAttribute("nails", nails);

		logger.info(">>>>>>>>>>>>>>>>> Controller userId : " + userId);
		logger.info(">>>>>>>>>>>>>>>>> Controller nailId : " + nailId);
		service.addWishList(userId, nailId);
		logger.info(">>>>>>>>>>>>>>>>> Controller service addWishList complete");
		
		/* List<WishList> wishLists = service.findAllWishLists(userId); */
		/* model.addAttribute("wishLists", wishLists); */
		return "mypage/cart";
	}
	
	@RequestMapping("/myWishList")
	public String list(String userId, String nailId, Model model) {
		List<Nail> nails = service.findAllWishLists(userId,nailId);
		model.addAttribute("nails", nails);
		
		return "mypage/cart";
	}
	
	@RequestMapping("/myWishList/remove")
	public String remove(int id) {
		service.removeWishList(id);
		return "mypage/cart";
	}
	
	
}
