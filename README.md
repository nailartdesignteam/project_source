[우선 수정이 필요한 부분]
- domain 클래스들 필드명 수정. (ex : nail_id => nailId)
- domain 필드들의 타입을 고민해 보고 수정
현재 대부분 String 타입이거나 int 타입이 전부인데 날짜와 같은 데이터는 java.sql 패키지의 Date 타입을 사용하는 것이 필요함.
- 프로젝트 폴더 구조 수정이 필요함.
nail 패키지 안에 domain, dao, controller... 과 같은 형태로 구성하는게 아님.
domain, controller, service, store 패키지는 하나로 구성.


* 관련 내용들에 관해 궁금한 사항 있으면 연락 할 것.